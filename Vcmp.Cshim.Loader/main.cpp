#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits.h>
#include <set>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include "coreclrhost.h"
#include "plugin.h"

#ifdef _WIN32
#define EXPORT __declspec(dllexport)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "direntwin.h"
#else
#define EXPORT
#define stricmp strcasecmp
#include <dirent.h>
#include <dlfcn.h>
#include <unistd.h>
#endif

typedef unsigned int (*bootstrap_ptr)(PluginFuncs* givenPluginFuncs, PluginCallbacks* givenPluginCalls, PluginInfo* givenPluginInfo);

void AddFilesFromDirectoryToTpaList(const char* directory, std::string& tpaList);

extern "C" EXPORT unsigned int VcmpPluginInit(
	PluginFuncs* givenPluginFuncs,
	PluginCallbacks* givenPluginCalls,
	PluginInfo* givenPluginInfo)
{
	char raw_app_path[PATH_MAX];
#ifdef _WIN32
	if (FAILED(GetCurrentDirectoryA(PATH_MAX, raw_app_path)))
#else
	if (getcwd(raw_app_path, PATH_MAX) == NULL)
#endif
	{
		fprintf(stderr, "Failed to get directory\n");
		return 0;
	}

	strcat(raw_app_path, "/plugins");

	char app_path[PATH_MAX];
#ifdef _WIN32
	strncpy(app_path, raw_app_path, PATH_MAX);
#else
	realpath(raw_app_path, app_path);
#endif

	FILE *config_file = fopen("server.cfg", "r");
	if (config_file == NULL) {
		fprintf(stderr, "Failed to get config\n");
		return 0;
	}

	char pkg_path[PATH_MAX];
	pkg_path[0] = '\0';

	char szLine[512];
	char szProperty[64];
	while (fgets(szLine, sizeof(szLine) / sizeof(char), config_file)) {
		sscanf(szLine, "%63s", szProperty);
		szProperty[63] = '\0';

		if (stricmp(szProperty, "coreclr_paths") == 0) {
			if (sscanf(szLine, "%63s %259[^\n]s", szProperty, pkg_path) == 2) {
				break;
			}
		}
	}

	if (pkg_path[0] == '\0') {
		fprintf(stderr, "Failed to get coreclr paths\n");
		return 0;
	}

	fclose(config_file);

	void *coreclr = NULL;
	coreclr_initialize_ptr coreclr_init = NULL;
	coreclr_create_delegate_ptr coreclr_create_dele = NULL;

#ifdef _WIN32
	char coreclr_path[PATH_MAX];
	sprintf(coreclr_path, "%s\\coreclr.dll", pkg_path);

	coreclr = LoadLibraryA(coreclr_path);
	coreclr_init = reinterpret_cast<coreclr_initialize_ptr>(GetProcAddress((HMODULE)coreclr, "coreclr_initialize"));
	coreclr_create_dele = reinterpret_cast<coreclr_create_delegate_ptr>(GetProcAddress((HMODULE)coreclr, "coreclr_create_delegate"));
#else
	coreclr = dlopen("libcoreclr.so", RTLD_NOW | RTLD_LOCAL);
	coreclr_init = reinterpret_cast<coreclr_initialize_ptr>(dlsym(coreclr, "coreclr_initialize"));
	coreclr_create_dele = reinterpret_cast<coreclr_create_delegate_ptr>(dlsym(coreclr, "coreclr_create_delegate"));
#endif

	if (!coreclr || !coreclr_init || !coreclr_create_dele) {
		fprintf(stderr, "Failed to get DLL handles\n");
		return 0;
	}

	std::string combined_paths;
	combined_paths = std::string(app_path) + ";" + std::string(pkg_path);

	std::string tpa_list;
	AddFilesFromDirectoryToTpaList(pkg_path, tpa_list);

	const char *property_keys[] = {
		"APP_PATHS",
		"APP_NI_PATHS",
		"NATIVE_DLL_SEARCH_DIRECTORIES",
		"TRUSTED_PLATFORM_ASSEMBLIES"
	};

	const char *property_values[] = {
		// APP_PATHS
		app_path,
		// APP_NI_PATHS
		app_path,
		// NATIVE_DLL_SEARCH_DIRECTORIES
		combined_paths.c_str(),
		// TRUSTED_PLATFORM_ASSEMBLIES
		tpa_list.c_str()
	};

	void *coreclr_handle;
	unsigned int domain_id;
	int ret = coreclr_init(
		app_path,                               // exePath
		"host",                                 // appDomainFriendlyName
		sizeof(property_values) / sizeof(char *), // propertyCount
		property_keys,                          // propertyKeys
		property_values,                        // propertyValues
		&coreclr_handle,                        // hostHandle
		&domain_id                              // domainId
	);

	bootstrap_ptr dele;
	ret = coreclr_create_dele(
		coreclr_handle,
		domain_id,
		"Vcmp.Cshim",
		"VcmpServer",
		"Bootstrap",
		reinterpret_cast<void **>(&dele)
	);

	if (dele == NULL) {
		fprintf(stderr, "Could not obtain a CoreCLR delegate, is Vcmp.Cshim present?\n");
		return 0;
	}

	return dele(givenPluginFuncs, givenPluginCalls, givenPluginInfo);
}

void AddFilesFromDirectoryToTpaList(const char* directory, std::string& tpaList)
{
	const char * const tpaExtensions[] = {
		".ni.dll",      // Probe for .ni.dll first so that it's preferred if ni and il coexist in the same dir
		".dll",
		".ni.exe",
		".exe",
	};

	DIR* dir = opendir(directory);
	if (dir == nullptr)
	{
		return;
	}

	std::set<std::string> addedAssemblies;

	// Walk the directory for each extension separately so that we first get files with .ni.dll extension,
	// then files with .dll extension, etc.
	for (int extIndex = 0; extIndex < sizeof(tpaExtensions) / sizeof(tpaExtensions[0]); extIndex++)
	{
		const char* ext = tpaExtensions[extIndex];
		int extLength = strlen(ext);

		struct dirent* entry;

		// For all entries in the directory
		while ((entry = readdir(dir)) != nullptr)
		{
			// We are interested in files only
			switch (entry->d_type)
			{
			case DT_REG:
				break;

			// Handle symlinks and file systems that do not support d_type
			case DT_UNKNOWN:
			{
				std::string fullFilename;

				fullFilename.append(directory);
				fullFilename.append("/");
				fullFilename.append(entry->d_name);

				struct stat sb;
				if (stat(fullFilename.c_str(), &sb) == -1)
				{
					continue;
				}

				if (!S_ISREG(sb.st_mode))
				{
					continue;
				}
			}
			break;

			default:
				continue;
			}

			std::string filename(entry->d_name);

			// Check if the extension matches the one we are looking for
			int extPos = filename.length() - extLength;
			if ((extPos <= 0) || (filename.compare(extPos, extLength, ext) != 0))
			{
				continue;
			}

			std::string filenameWithoutExt(filename.substr(0, extPos));

			// Make sure if we have an assembly with multiple extensions present,
			// we insert only one version of it.
			if (addedAssemblies.find(filenameWithoutExt) == addedAssemblies.end())
			{
				addedAssemblies.insert(filenameWithoutExt);

				tpaList.append(directory);
				tpaList.append("/");
				tpaList.append(filename);
				tpaList.append(";");
			}
		}

		// Rewind the directory stream to be able to iterate over it for the next extension
		rewinddir(dir);
	}

	closedir(dir);
}