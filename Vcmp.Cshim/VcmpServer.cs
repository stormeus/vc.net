﻿// <copyright file="VcmpServer.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using Vcmp.Cshim.Core;
using Vcmp.Cshim.Internal;
using Vcmp.Cshim.Models;

/// <summary>
/// Entry point for the C# VCMP server controller code.
/// Shouldn't need to be modified by the server itself.
/// </summary>
public static class VcmpServer
{
    private static Director director;

    internal static PluginFuncs Functions { get; set; }

    /// <summary>
    /// Entry point called by the loader for the server plugin
    /// </summary>
    /// <param name="functions">The functions API</param>
    /// <param name="events">A reference to the plugin events struct</param>
    /// <param name="info">A reference to the plugin info struct</param>
    /// <returns>1 if init succeeded, 0 otherwise</returns>
    internal static uint Bootstrap(
        PluginFuncs functions,
        ref PluginEvents events,
        ref PluginInfo info)
    {
        info.PluginVersion = 0x1000;
        info.ApiMajorVersion = 2;
        info.ApiMinorVersion = 0;

        VcmpServer.director = new Director(functions, ref events);
        VcmpServer.Functions = functions;
        return 1;
    }
}
