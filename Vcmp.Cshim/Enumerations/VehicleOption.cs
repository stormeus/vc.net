﻿// <copyright file="VehicleOption.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Types of vehicle options that can be checked and configured
    /// </summary>
    public enum VehicleOption : int
    {
        /// <summary>
        /// Doors of the vehicle are locked
        /// </summary>
        DoorsLocked = 0,

        /// <summary>
        /// Vehicle alarm active
        /// </summary>
        Alarm,

        /// <summary>
        /// Vehicle lights active
        /// </summary>
        Lights,

        /// <summary>
        /// Radio station is locked and cannot be changed
        /// </summary>
        RadioLocked,

        /// <summary>
        /// Vehicle does not have collision
        /// </summary>
        Ghost,

        /// <summary>
        /// Siren is active
        /// </summary>
        Siren
    }
}
