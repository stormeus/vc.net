﻿// <copyright file="PlayerOption.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Options that can be checked and configured for players
    /// </summary>
    public enum PlayerOption : int
    {
        /// <summary>
        /// Can move and use controls
        /// </summary>
        Controllable = 0,

        /// <summary>
        /// Can use driveby
        /// </summary>
        DriveBy,

        /// <summary>
        /// White scanlines rendered on screen
        /// </summary>
        WhiteScanlines,

        /// <summary>
        /// Green scanlines rendered on screen
        /// </summary>
        GreenScanlines,

        /// <summary>
        /// Widescreen bars rendered and the HUD disappears
        /// </summary>
        Widescreen,

        /// <summary>
        /// Markers shown on the player's minimap
        /// </summary>
        ShowMarkers,

        /// <summary>
        /// Can use the fire button and attack players
        /// </summary>
        CanAttack,

        /// <summary>
        /// Player's marker is visible to everyone else who can see markers
        /// </summary>
        HasMarker,

        /// <summary>
        /// Player can use chat tags when typing messages (i.e. for color codes)
        /// </summary>
        ChatTagsEnabled,

        /// <summary>
        /// Player has drunk camera effects
        /// </summary>
        DrunkEffects
    }
}
