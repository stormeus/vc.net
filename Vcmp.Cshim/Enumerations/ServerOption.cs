﻿// <copyright file="ServerOption.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Server options that can be checked and configured
    /// </summary>
    public enum ServerOption : int
    {
        /// <summary>
        /// Frame limiter settings are synced and forced for all players
        /// </summary>
        SyncFrameLimiter = 0,

        /// <summary>
        /// Frame limiter is enabled
        /// </summary>
        FrameLimiter,

        /// <summary>
        /// Players can boost-jump zebra taxis
        /// </summary>
        TaxiBoostJump,

        /// <summary>
        /// All vehicles drive on water
        /// </summary>
        DriveOnWater,

        /// <summary>
        /// Players can fast-switch in the middle of a shot
        /// </summary>
        FastSwitch,

        /// <summary>
        /// Players on the same team deal damage to each other
        /// </summary>
        FriendlyFire,

        /// <summary>
        /// Driveby is disabled for all players
        /// </summary>
        DisableDriveBy,

        /// <summary>
        /// Cars handle perfectly (i.e. the GRIPISEVERYTHING cheat)
        /// </summary>
        PerfectHandling,

        /// <summary>
        /// Cars can fly (i.e. the COMEFLYWITHME cheat)
        /// </summary>
        FlyingCars,

        /// <summary>
        /// Players can switch weapons while falling through the air
        /// </summary>
        JumpSwitch,

        /// <summary>
        /// Show minimap blips for players
        /// </summary>
        ShowMarkers,

        /// <summary>
        /// Players only see minimap blips for players on their own team
        /// </summary>
        OnlyShowTeamMarkers,

        /// <summary>
        /// Players do not fall off bikes when stunting
        /// </summary>
        StuntBike,

        /// <summary>
        /// Players can shoot weapons while falling through the air
        /// </summary>
        ShootInAir,

        /// <summary>
        /// Nametags are rendered for players
        /// </summary>
        ShowNameTags,

        /// <summary>
        /// Messages are automatically shown by the server when players join
        /// </summary>
        JoinMessages,

        /// <summary>
        /// Messages are automatically shown by the server when players die
        /// </summary>
        DeathMessages,

        /// <summary>
        /// Players can use color tags in chat
        /// </summary>
        ChatTagsEnabled,

        /// <summary>
        /// Player classes are used and can be added
        /// </summary>
        UseClasses,

        /// <summary>
        /// Wallglitch is enabled
        /// </summary>
        WallGlitch,

        /// <summary>
        /// Textures are forced to appear even when behind a solid building with no
        /// textures on the opposite side. Mostly used for GTA3 maps, not recommended
        /// to disable this optimization.
        /// </summary>
        DisableBackfaceCulling,

        /// <summary>
        /// Heliblades kill players
        /// </summary>
        DisableHeliBladeDamage
    }
}
