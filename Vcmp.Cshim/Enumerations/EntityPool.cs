﻿// <copyright file="EntityPool.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Types of entities in an entity pool
    /// </summary>
    public enum EntityPool : int
    {
        /// <summary>
        /// Vehicle entity
        /// </summary>
        Vehicle = 1,

        /// <summary>
        /// Object entity
        /// </summary>
        Object,

        /// <summary>
        /// Pickup entity
        /// </summary>
        Pickup,

        /// <summary>
        /// Radio entity
        /// </summary>
        Radio,

        /// <summary>
        /// Radar blip entity
        /// </summary>
        Blip,

        /// <summary>
        /// Checkpoint/sphere entity
        /// </summary>
        Checkpoint
    }
}
