﻿// <copyright file="ErrorCode.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// API error codes
    /// </summary>
    public enum ErrorCode : int
    {
        /// <summary>
        /// No error code, operation succeeded
        /// </summary>
        None = 0,

        /// <summary>
        /// The entity specified does not exist
        /// </summary>
        NoSuchEntity,

        /// <summary>
        /// The buffer given was too small
        /// </summary>
        BufferTooSmall,

        /// <summary>
        /// The input buffer was too large
        /// </summary>
        TooLargeInput,

        /// <summary>
        /// The argument was outside the acceptable range of parameters
        /// </summary>
        ArgumentOutOfBounds,

        /// <summary>
        /// The argument given was null when an actual param was expected
        /// </summary>
        NullArgument,

        /// <summary>
        /// There is no more space in the entity pool to fulfill the create request
        /// </summary>
        PoolExhausted,

        /// <summary>
        /// The name given contains invalid characters
        /// </summary>
        InvalidName,

        /// <summary>
        /// The request was denied - for vehicles, there may not be enough room
        /// </summary>
        RequestDenied
    }
}
