﻿// <copyright file="VehicleUpdate.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Types of vehicle updates
    /// </summary>
    public enum VehicleUpdate : int
    {
        /// <summary>
        /// Sync update from a driver
        /// </summary>
        DriverSync = 0,
        
        /// <summary>
        /// Sync update from a passenger or nearby player
        /// </summary>
        OtherSync,
        
        /// <summary>
        /// Position changed
        /// </summary>
        Position,

        /// <summary>
        /// Health changed
        /// </summary>
        Health,

        /// <summary>
        /// Colour changed
        /// </summary>
        Colour,

        /// <summary>
        /// Rotation changed
        /// </summary>
        Rotation,
    }
}
