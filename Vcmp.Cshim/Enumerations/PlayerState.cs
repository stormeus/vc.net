﻿// <copyright file="PlayerState.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// States a player can be in at any given time
    /// </summary>
    public enum PlayerState : int
    {
        /// <summary>
        /// Player does not exist
        /// </summary>
        None = 0,

        /// <summary>
        /// Player is on-foot
        /// </summary>
        Normal,

        /// <summary>
        /// Aiming in first-person
        /// </summary>
        Aim,

        /// <summary>
        /// Driving a vehicle
        /// </summary>
        Driver,

        /// <summary>
        /// Passenger in a vehicle
        /// </summary>
        Passenger,

        /// <summary>
        /// Entering a vehicle as a driver
        /// </summary>
        EnterDriver,

        /// <summary>
        /// Entering a vehicle as a passenger
        /// </summary>
        EnterPassenger,

        /// <summary>
        /// Exiting a vehicle
        /// </summary>
        Exit,

        /// <summary>
        /// Not spawned
        /// </summary>
        Unspawned
    }
}
