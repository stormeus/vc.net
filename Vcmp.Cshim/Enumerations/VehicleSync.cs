﻿// <copyright file="VehicleSync.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Different types of vehicle sync packets
    /// </summary>
    public enum VehicleSync : int
    {
        /// <summary>
        /// Vehicle is not synced or does not exist
        /// </summary>
        None = 0,

        /// <summary>
        /// Vehicle sync is primarily from driver
        /// </summary>
        Driver,

        /// <summary>
        /// Vehicle sync is primarily from passenger
        /// </summary>
        Passenger = 3,

        /// <summary>
        /// Vehicle sync is primarily from a nearby player
        /// </summary>
        Near
    }
}
