﻿// <copyright file="PlayerVehicle.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Player states relative to vehicles
    /// </summary>
    public enum PlayerVehicle : int
    {
        /// <summary>
        /// Not in a vehicle
        /// </summary>
        Out = 0,

        /// <summary>
        /// Entering a vehicle
        /// </summary>
        Entering,

        /// <summary>
        /// Exiting a vehicle
        /// </summary>
        Exiting,

        /// <summary>
        /// In a vehicle
        /// </summary>
        In
    }
}
