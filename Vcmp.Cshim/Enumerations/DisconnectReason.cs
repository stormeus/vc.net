﻿// <copyright file="DisconnectReason.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Reasons a player may have disconnected
    /// </summary>
    public enum DisconnectReason : int
    {
        /// <summary>
        /// Player timed out due to lost connection or disconnect without explicit quit
        /// </summary>
        Timeout = 0,

        /// <summary>
        /// Player explicitly quit with /q or exit menu
        /// </summary>
        Quit,

        /// <summary>
        /// Player was kicked by the server
        /// </summary>
        Kick,

        /// <summary>
        /// Player crashed
        /// </summary>
        Crash,

        /// <summary>
        /// Player violated an anti-cheat rule
        /// </summary>
        AntiCheat
    }
}
