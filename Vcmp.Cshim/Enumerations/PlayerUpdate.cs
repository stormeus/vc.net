﻿// <copyright file="PlayerUpdate.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Types of player sync packets
    /// </summary>
    public enum PlayerUpdate : int
    {
        /// <summary>
        /// Foot sync
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Aiming in first-person
        /// </summary>
        Aiming,

        /// <summary>
        /// Driving a vehicle
        /// </summary>
        Driver,

        /// <summary>
        /// Passenger in a vehicle
        /// </summary>
        Passenger
    }
}
