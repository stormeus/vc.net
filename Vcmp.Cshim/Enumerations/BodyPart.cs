﻿// <copyright file="BodyPart.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Enumerations
{
    /// <summary>
    /// Player body parts, primarily used for death events
    /// </summary>
    public enum BodyPart : int
    {
        /// <summary>
        /// Body - catch-all if no other parts were shot
        /// </summary>
        Body = 0,

        /// <summary>
        /// Torso
        /// </summary>
        Torso,

        /// <summary>
        /// Left arm
        /// </summary>
        LeftArm,

        /// <summary>
        /// Right arm
        /// </summary>
        RightArm,

        /// <summary>
        /// Left leg
        /// </summary>
        LeftLeg,

        /// <summary>
        /// Right leg
        /// </summary>
        RightLeg,

        /// <summary>
        /// Head
        /// </summary>
        Head,

        /// <summary>
        /// Player was shot in a vehicle
        /// </summary>
        InVehicle
    }
}
