﻿// <copyright file="PluginFuncs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Internal
{
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    internal struct PluginFuncs
    {
#pragma warning disable SA1600 // Elements must be documented
        public uint StructSize;

        public PluginFuncDelegates.P_GetServerVersion GetServerVersion;
        public PluginFuncDelegates.P_GetServerSettings GetServerSettings;
        public PluginFuncDelegates.P_ExportFunctions ExportFunctions;
        public PluginFuncDelegates.P_GetNumberOfPlugins GetNumberOfPlugins;
        public PluginFuncDelegates.P_GetPluginInfo GetPluginInfo;
        public PluginFuncDelegates.P_FindPlugin FindPlugin;
        public PluginFuncDelegates.P_GetPluginExports GetPluginExports;
        public PluginFuncDelegates.P_SendPluginCommand SendPluginCommand;
        public PluginFuncDelegates.P_GetTime GetTime;
        public PluginFuncDelegates.P_LogMessage LogMessage;
        public PluginFuncDelegates.P_GetLastError GetLastError;

        // Client messages
        public PluginFuncDelegates.P_SendClientScriptData SendClientScriptData;
        public PluginFuncDelegates.P_SendClientMessage SendClientMessage;
        public PluginFuncDelegates.P_SendGameMessage SendGameMessage;

        // Server settings
        public PluginFuncDelegates.P_SetServerName SetServerName;
        public PluginFuncDelegates.P_GetServerName GetServerName;
        public PluginFuncDelegates.P_SetMaxPlayers SetMaxPlayers;
        public PluginFuncDelegates.P_GetMaxPlayers GetMaxPlayers;
        public PluginFuncDelegates.P_SetServerPassword SetServerPassword;
        public PluginFuncDelegates.P_GetServerPassword GetServerPassword;
        public PluginFuncDelegates.P_SetGameModeText SetGameModeText;
        public PluginFuncDelegates.P_GetGameModeText GetGameModeText;
        public PluginFuncDelegates.P_ShutdownServer ShutdownServer;

        // Game environment settings
        public PluginFuncDelegates.P_SetServerOption SetServerOption;
        public PluginFuncDelegates.P_GetServerOption GetServerOption;
        public PluginFuncDelegates.P_SetWorldBounds SetWorldBounds;
        public PluginFuncDelegates.P_GetWorldBounds GetWorldBounds;
        public PluginFuncDelegates.P_SetWastedSettings SetWastedSettings;
        public PluginFuncDelegates.P_GetWastedSettings GetWastedSettings;
        public PluginFuncDelegates.P_SetTimeRate SetTimeRate;
        public PluginFuncDelegates.P_GetTimeRate GetTimeRate;
        public PluginFuncDelegates.P_SetHour SetHour;
        public PluginFuncDelegates.P_GetHour GetHour;
        public PluginFuncDelegates.P_SetMinute SetMinute;
        public PluginFuncDelegates.P_GetMinute GetMinute;
        public PluginFuncDelegates.P_SetWeather SetWeather;
        public PluginFuncDelegates.P_GetWeather GetWeather;
        public PluginFuncDelegates.P_SetGravity SetGravity;
        public PluginFuncDelegates.P_GetGravity GetGravity;
        public PluginFuncDelegates.P_SetGameSpeed SetGameSpeed;
        public PluginFuncDelegates.P_GetGameSpeed GetGameSpeed;
        public PluginFuncDelegates.P_SetWaterLevel SetWaterLevel;
        public PluginFuncDelegates.P_GetWaterLevel GetWaterLevel;
        public PluginFuncDelegates.P_SetMaximumFlightAltitude SetMaximumFlightAltitude;
        public PluginFuncDelegates.P_GetMaximumFlightAltitude GetMaximumFlightAltitude;
        public PluginFuncDelegates.P_SetKillCommandDelay SetKillCommandDelay;
        public PluginFuncDelegates.P_GetKillCommandDelay GetKillCommandDelay;
        public PluginFuncDelegates.P_SetVehiclesForcedRespawnHeight SetVehiclesForcedRespawnHeight;
        public PluginFuncDelegates.P_GetVehiclesForcedRespawnHeight GetVehiclesForcedRespawnHeight;

        // Miscellaneous things
        public PluginFuncDelegates.P_CreateExplosion CreateExplosion;
        public PluginFuncDelegates.P_PlaySound PlaySound;
        public PluginFuncDelegates.P_HideMapObject HideMapObject;
        public PluginFuncDelegates.P_ShowMapObject ShowMapObject;
        public PluginFuncDelegates.P_ShowAllMapObjects ShowAllMapObjects;

        // Weapon settings
        public PluginFuncDelegates.P_SetWeaponDataValue SetWeaponDataValue;
        public PluginFuncDelegates.P_GetWeaponDataValue GetWeaponDataValue;
        public PluginFuncDelegates.P_ResetWeaponDataValue ResetWeaponDataValue;
        public PluginFuncDelegates.P_IsWeaponDataValueModified IsWeaponDataValueModified;
        public PluginFuncDelegates.P_ResetWeaponData ResetWeaponData;
        public PluginFuncDelegates.P_ResetAllWeaponData ResetAllWeaponData;

        // Key binds
        public PluginFuncDelegates.P_GetKeyBindUnusedSlot GetKeyBindUnusedSlot;
        public PluginFuncDelegates.P_GetKeyBindData GetKeyBindData;
        public PluginFuncDelegates.P_RegisterKeyBind RegisterKeyBind;
        public PluginFuncDelegates.P_RemoveKeyBind RemoveKeyBind;
        public PluginFuncDelegates.P_RemoveAllKeyBinds RemoveAllKeyBinds;

        // Coordinate blips
        public PluginFuncDelegates.P_CreateCoordBlip CreateCoordBlip;
        public PluginFuncDelegates.P_DestroyCoordBlip DestroyCoordBlip;
        public PluginFuncDelegates.P_GetCoordBlipInfo GetCoordBlipInfo;

        // Radios
        public PluginFuncDelegates.P_AddRadioStream AddRadioStream;
        public PluginFuncDelegates.P_RemoveRadioStream RemoveRadioStream;

        // Spawning and classes
        public PluginFuncDelegates.P_AddPlayerClass AddPlayerClass;
        public PluginFuncDelegates.P_SetSpawnPlayerPosition SetSpawnPlayerPosition;
        public PluginFuncDelegates.P_SetSpawnCameraPosition SetSpawnCameraPosition;
        public PluginFuncDelegates.P_SetSpawnCameraLookAt SetSpawnCameraLookAt;

        // Administration
        public PluginFuncDelegates.P_IsPlayerAdmin IsPlayerAdmin;
        public PluginFuncDelegates.P_SetPlayerAdmin SetPlayerAdmin;
        public PluginFuncDelegates.P_GetPlayerIP GetPlayerIP;
        public PluginFuncDelegates.P_GetPlayerUID GetPlayerUID;
        public PluginFuncDelegates.P_GetPlayerUID2 GetPlayerUID2;
        public PluginFuncDelegates.P_KickPlayer KickPlayer;
        public PluginFuncDelegates.P_BanPlayer BanPlayer;
        public PluginFuncDelegates.P_BanIP BanIP;
        public PluginFuncDelegates.P_UnbanIP UnbanIP;
        public PluginFuncDelegates.P_IsIPBanned IsIPBanned;

        // Player access and basic info
        public PluginFuncDelegates.P_GetPlayerIdFromName GetPlayerIdFromName;
        public PluginFuncDelegates.P_IsPlayerConnected IsPlayerConnected;
        public PluginFuncDelegates.P_IsPlayerStreamedForPlayer IsPlayerStreamedForPlayer;
        public PluginFuncDelegates.P_GetPlayerKey GetPlayerKey;
        public PluginFuncDelegates.P_GetPlayerName GetPlayerName;
        public PluginFuncDelegates.P_SetPlayerName SetPlayerName;
        public PluginFuncDelegates.P_GetPlayerState GetPlayerState;
        public PluginFuncDelegates.P_SetPlayerOption SetPlayerOption;
        public PluginFuncDelegates.P_GetPlayerOption GetPlayerOption;

        // Player world
        public PluginFuncDelegates.P_SetPlayerWorld SetPlayerWorld;
        public PluginFuncDelegates.P_GetPlayerWorld GetPlayerWorld;
        public PluginFuncDelegates.P_SetPlayerSecondaryWorld SetPlayerSecondaryWorld;
        public PluginFuncDelegates.P_GetPlayerSecondaryWorld GetPlayerSecondaryWorld;
        public PluginFuncDelegates.P_GetPlayerUniqueWorld GetPlayerUniqueWorld;
        public PluginFuncDelegates.P_IsPlayerWorldCompatible IsPlayerWorldCompatible;

        // Player class, team, skin, colour
        public PluginFuncDelegates.P_GetPlayerClass GetPlayerClass;
        public PluginFuncDelegates.P_SetPlayerTeam SetPlayerTeam;
        public PluginFuncDelegates.P_GetPlayerTeam GetPlayerTeam;
        public PluginFuncDelegates.P_SetPlayerSkin SetPlayerSkin;
        public PluginFuncDelegates.P_GetPlayerSkin GetPlayerSkin;
        public PluginFuncDelegates.P_SetPlayerColour SetPlayerColour;
        public PluginFuncDelegates.P_GetPlayerColour GetPlayerColour;

        // Player spawn cycle
        public PluginFuncDelegates.P_IsPlayerSpawned IsPlayerSpawned;
        public PluginFuncDelegates.P_ForcePlayerSpawn ForcePlayerSpawn;
        public PluginFuncDelegates.P_ForcePlayerSelect ForcePlayerSelect;
        public PluginFuncDelegates.P_ForceAllSelect ForceAllSelect;
        public PluginFuncDelegates.P_IsPlayerTyping IsPlayerTyping;

        // Player money, score, wanted level
        public PluginFuncDelegates.P_GivePlayerMoney GivePlayerMoney;
        public PluginFuncDelegates.P_SetPlayerMoney SetPlayerMoney;
        public PluginFuncDelegates.P_GetPlayerMoney GetPlayerMoney;
        public PluginFuncDelegates.P_SetPlayerScore SetPlayerScore;
        public PluginFuncDelegates.P_GetPlayerScore GetPlayerScore;
        public PluginFuncDelegates.P_SetPlayerWantedLevel SetPlayerWantedLevel;
        public PluginFuncDelegates.P_GetPlayerWantedLevel GetPlayerWantedLevel;
        public PluginFuncDelegates.P_GetPlayerPing GetPlayerPing;
        public PluginFuncDelegates.P_GetPlayerFPS GetPlayerFPS;

        // Player health and immunity
        public PluginFuncDelegates.P_SetPlayerHealth SetPlayerHealth;
        public PluginFuncDelegates.P_GetPlayerHealth GetPlayerHealth;
        public PluginFuncDelegates.P_SetPlayerArmour SetPlayerArmour;
        public PluginFuncDelegates.P_GetPlayerArmour GetPlayerArmour;
        public PluginFuncDelegates.P_SetPlayerImmunityFlags SetPlayerImmunityFlags;
        public PluginFuncDelegates.P_GetPlayerImmunityFlags GetPlayerImmunityFlags;

        // Player position and rotation
        public PluginFuncDelegates.P_SetPlayerPosition SetPlayerPosition;
        public PluginFuncDelegates.P_GetPlayerPosition GetPlayerPosition;
        public PluginFuncDelegates.P_SetPlayerSpeed SetPlayerSpeed;
        public PluginFuncDelegates.P_GetPlayerSpeed GetPlayerSpeed;
        public PluginFuncDelegates.P_AddPlayerSpeed AddPlayerSpeed;
        public PluginFuncDelegates.P_SetPlayerHeading SetPlayerHeading;
        public PluginFuncDelegates.P_GetPlayerHeading GetPlayerHeading;
        public PluginFuncDelegates.P_SetPlayerAlpha SetPlayerAlpha;
        public PluginFuncDelegates.P_GetPlayerAlpha GetPlayerAlpha;
        public PluginFuncDelegates.P_GetPlayerAimPosition GetPlayerAimPosition;
        public PluginFuncDelegates.P_GetPlayerAimDirection GetPlayerAimDirection;

        // Player actions and keys
        public PluginFuncDelegates.P_IsPlayerOnFire IsPlayerOnFire;
        public PluginFuncDelegates.P_IsPlayerCrouching IsPlayerCrouching;
        public PluginFuncDelegates.P_GetPlayerAction GetPlayerAction;
        public PluginFuncDelegates.P_GetPlayerGameKeys GetPlayerGameKeys;

        // Player vehicle
        public PluginFuncDelegates.P_PutPlayerInVehicle PutPlayerInVehicle;
        public PluginFuncDelegates.P_RemovePlayerFromVehicle RemovePlayerFromVehicle;
        public PluginFuncDelegates.P_GetPlayerInVehicleStatus GetPlayerInVehicleStatus;
        public PluginFuncDelegates.P_GetPlayerInVehicleSlot GetPlayerInVehicleSlot;
        public PluginFuncDelegates.P_GetPlayerVehicleId GetPlayerVehicleId;

        // Player weapons
        public PluginFuncDelegates.P_GivePlayerWeapon GivePlayerWeapon;
        public PluginFuncDelegates.P_SetPlayerWeapon SetPlayerWeapon;
        public PluginFuncDelegates.P_GetPlayerWeapon GetPlayerWeapon;
        public PluginFuncDelegates.P_GetPlayerWeaponAmmo GetPlayerWeaponAmmo;
        public PluginFuncDelegates.P_SetPlayerWeaponSlot SetPlayerWeaponSlot;
        public PluginFuncDelegates.P_GetPlayerWeaponSlot GetPlayerWeaponSlot;
        public PluginFuncDelegates.P_GetPlayerWeaponAtSlot GetPlayerWeaponAtSlot;
        public PluginFuncDelegates.P_GetPlayerAmmoAtSlot GetPlayerAmmoAtSlot;
        public PluginFuncDelegates.P_RemovePlayerWeapon RemovePlayerWeapon;
        public PluginFuncDelegates.P_RemoveAllWeapons RemoveAllWeapons;

        // Player camera
        public PluginFuncDelegates.P_SetCameraPosition SetCameraPosition;
        public PluginFuncDelegates.P_RestoreCamera RestoreCamera;
        public PluginFuncDelegates.P_IsCameraLocked IsCameraLocked;

        // Player miscellaneous stuff
        public PluginFuncDelegates.P_SetPlayerAnimation SetPlayerAnimation;
        public PluginFuncDelegates.P_GetPlayerStandingOnVehicle GetPlayerStandingOnVehicle;
        public PluginFuncDelegates.P_GetPlayerStandingOnObject GetPlayerStandingOnObject;
        public PluginFuncDelegates.P_IsPlayerAway IsPlayerAway;
        public PluginFuncDelegates.P_GetPlayerSpectateTarget GetPlayerSpectateTarget;
        public PluginFuncDelegates.P_SetPlayerSpectateTarget SetPlayerSpectateTarget;
        public PluginFuncDelegates.P_RedirectPlayerToServer RedirectPlayerToServer;

        // All entities
        public PluginFuncDelegates.P_CheckEntityExists CheckEntityExists;

        // Vehicles
        public PluginFuncDelegates.P_CreateVehicle CreateVehicle;
        public PluginFuncDelegates.P_DeleteVehicle DeleteVehicle;
        public PluginFuncDelegates.P_SetVehicleOption SetVehicleOption;
        public PluginFuncDelegates.P_GetVehicleOption GetVehicleOption;
        public PluginFuncDelegates.P_GetVehicleSyncSource GetVehicleSyncSource;
        public PluginFuncDelegates.P_GetVehicleSyncType GetVehicleSyncType;
        public PluginFuncDelegates.P_IsVehicleStreamedForPlayer IsVehicleStreamedForPlayer;
        public PluginFuncDelegates.P_SetVehicleWorld SetVehicleWorld;
        public PluginFuncDelegates.P_GetVehicleWorld GetVehicleWorld;
        public PluginFuncDelegates.P_GetVehicleModel GetVehicleModel;
        public PluginFuncDelegates.P_GetVehicleOccupant GetVehicleOccupant;
        public PluginFuncDelegates.P_RespawnVehicle RespawnVehicle;
        public PluginFuncDelegates.P_SetVehicleImmunityFlags SetVehicleImmunityFlags;
        public PluginFuncDelegates.P_GetVehicleImmunityFlags GetVehicleImmunityFlags;
        public PluginFuncDelegates.P_ExplodeVehicle ExplodeVehicle;
        public PluginFuncDelegates.P_IsVehicleWrecked IsVehicleWrecked;
        public PluginFuncDelegates.P_SetVehiclePosition SetVehiclePosition;
        public PluginFuncDelegates.P_GetVehiclePosition GetVehiclePosition;
        public PluginFuncDelegates.P_SetVehicleRotation SetVehicleRotation;
        public PluginFuncDelegates.P_SetVehicleRotationEuler SetVehicleRotationEuler;
        public PluginFuncDelegates.P_GetVehicleRotation GetVehicleRotation;
        public PluginFuncDelegates.P_GetVehicleRotationEuler GetVehicleRotationEuler;
        public PluginFuncDelegates.P_SetVehicleSpeed SetVehicleSpeed;
        public PluginFuncDelegates.P_GetVehicleSpeed GetVehicleSpeed;
        public PluginFuncDelegates.P_SetVehicleTurnSpeed SetVehicleTurnSpeed;
        public PluginFuncDelegates.P_GetVehicleTurnSpeed GetVehicleTurnSpeed;
        public PluginFuncDelegates.P_SetVehicleSpawnPosition SetVehicleSpawnPosition;
        public PluginFuncDelegates.P_GetVehicleSpawnPosition GetVehicleSpawnPosition;
        public PluginFuncDelegates.P_SetVehicleSpawnRotation SetVehicleSpawnRotation;
        public PluginFuncDelegates.P_SetVehicleSpawnRotationEuler SetVehicleSpawnRotationEuler;
        public PluginFuncDelegates.P_GetVehicleSpawnRotation GetVehicleSpawnRotation;
        public PluginFuncDelegates.P_GetVehicleSpawnRotationEuler GetVehicleSpawnRotationEuler;
        public PluginFuncDelegates.P_SetVehicleIdleRespawnTimer SetVehicleIdleRespawnTimer;
        public PluginFuncDelegates.P_GetVehicleIdleRespawnTimer GetVehicleIdleRespawnTimer;
        public PluginFuncDelegates.P_SetVehicleHealth SetVehicleHealth;
        public PluginFuncDelegates.P_GetVehicleHealth GetVehicleHealth;
        public PluginFuncDelegates.P_SetVehicleColour SetVehicleColour;
        public PluginFuncDelegates.P_GetVehicleColour GetVehicleColour;
        public PluginFuncDelegates.P_SetVehiclePartStatus SetVehiclePartStatus;
        public PluginFuncDelegates.P_GetVehiclePartStatus GetVehiclePartStatus;
        public PluginFuncDelegates.P_SetVehicleTyreStatus SetVehicleTyreStatus;
        public PluginFuncDelegates.P_GetVehicleTyreStatus GetVehicleTyreStatus;
        public PluginFuncDelegates.P_SetVehicleDamageData SetVehicleDamageData;
        public PluginFuncDelegates.P_GetVehicleDamageData GetVehicleDamageData;
        public PluginFuncDelegates.P_SetVehicleRadio SetVehicleRadio;
        public PluginFuncDelegates.P_GetVehicleRadio GetVehicleRadio;
        public PluginFuncDelegates.P_GetVehicleTurretRotation GetVehicleTurretRotation;

        // Vehicle handling
        public PluginFuncDelegates.P_ResetAllVehicleHandlings ResetAllVehicleHandlings;
        public PluginFuncDelegates.P_ExistsHandlingRule ExistsHandlingRule;
        public PluginFuncDelegates.P_SetHandlingRule SetHandlingRule;
        public PluginFuncDelegates.P_GetHandlingRule GetHandlingRule;
        public PluginFuncDelegates.P_ResetHandlingRule ResetHandlingRule;
        public PluginFuncDelegates.P_ResetHandling ResetHandling;
        public PluginFuncDelegates.P_ExistsInstHandlingRule ExistsInstHandlingRule;
        public PluginFuncDelegates.P_SetInstHandlingRule SetInstHandlingRule;
        public PluginFuncDelegates.P_GetInstHandlingRule GetInstHandlingRule;
        public PluginFuncDelegates.P_ResetInstHandlingRule ResetInstHandlingRule;
        public PluginFuncDelegates.P_ResetInstHandling ResetInstHandling;

        // Pickups
        public PluginFuncDelegates.P_CreatePickup CreatePickup;
        public PluginFuncDelegates.P_DeletePickup DeletePickup;
        public PluginFuncDelegates.P_IsPickupStreamedForPlayer IsPickupStreamedForPlayer;
        public PluginFuncDelegates.P_SetPickupWorld SetPickupWorld;
        public PluginFuncDelegates.P_GetPickupWorld GetPickupWorld;
        public PluginFuncDelegates.P_SetPickupAlpha SetPickupAlpha;
        public PluginFuncDelegates.P_GetPickupAlpha GetPickupAlpha;
        public PluginFuncDelegates.P_SetPickupIsAutomatic SetPickupIsAutomatic;
        public PluginFuncDelegates.P_IsPickupAutomatic IsPickupAutomatic;
        public PluginFuncDelegates.P_SetPickupAutoTimer SetPickupAutoTimer;
        public PluginFuncDelegates.P_GetPickupAutoTimer GetPickupAutoTimer;
        public PluginFuncDelegates.P_RefreshPickup RefreshPickup;
        public PluginFuncDelegates.P_SetPickupPosition SetPickupPosition;
        public PluginFuncDelegates.P_GetPickupPosition GetPickupPosition;
        public PluginFuncDelegates.P_GetPickupModel GetPickupModel;
        public PluginFuncDelegates.P_GetPickupQuantity GetPickupQuantity;

        // Checkpoints
        public PluginFuncDelegates.P_CreateCheckPoint CreateCheckPoint;
        public PluginFuncDelegates.P_DeleteCheckPoint DeleteCheckPoint;
        public PluginFuncDelegates.P_IsCheckPointStreamedForPlayer IsCheckPointStreamedForPlayer;
        public PluginFuncDelegates.P_IsCheckPointSphere IsCheckPointSphere;
        public PluginFuncDelegates.P_SetCheckPointWorld SetCheckPointWorld;
        public PluginFuncDelegates.P_GetCheckPointWorld GetCheckPointWorld;
        public PluginFuncDelegates.P_SetCheckPointColour SetCheckPointColour;
        public PluginFuncDelegates.P_GetCheckPointColour GetCheckPointColour;
        public PluginFuncDelegates.P_SetCheckPointPosition SetCheckPointPosition;
        public PluginFuncDelegates.P_GetCheckPointPosition GetCheckPointPosition;
        public PluginFuncDelegates.P_SetCheckPointRadius SetCheckPointRadius;
        public PluginFuncDelegates.P_GetCheckPointRadius GetCheckPointRadius;
        public PluginFuncDelegates.P_GetCheckPointOwner GetCheckPointOwner;

        // Objects
        public PluginFuncDelegates.P_CreateObject CreateObject;
        public PluginFuncDelegates.P_DeleteObject DeleteObject;
        public PluginFuncDelegates.P_IsObjectStreamedForPlayer IsObjectStreamedForPlayer;
        public PluginFuncDelegates.P_GetObjectModel GetObjectModel;
        public PluginFuncDelegates.P_SetObjectWorld SetObjectWorld;
        public PluginFuncDelegates.P_GetObjectWorld GetObjectWorld;
        public PluginFuncDelegates.P_SetObjectAlpha SetObjectAlpha;
        public PluginFuncDelegates.P_GetObjectAlpha GetObjectAlpha;
        public PluginFuncDelegates.P_MoveObjectTo MoveObjectTo;
        public PluginFuncDelegates.P_MoveObjectBy MoveObjectBy;
        public PluginFuncDelegates.P_SetObjectPosition SetObjectPosition;
        public PluginFuncDelegates.P_GetObjectPosition GetObjectPosition;
        public PluginFuncDelegates.P_RotateObjectTo RotateObjectTo;
        public PluginFuncDelegates.P_RotateObjectToEuler RotateObjectToEuler;
        public PluginFuncDelegates.P_RotateObjectBy RotateObjectBy;
        public PluginFuncDelegates.P_RotateObjectByEuler RotateObjectByEuler;
        public PluginFuncDelegates.P_GetObjectRotation GetObjectRotation;
        public PluginFuncDelegates.P_GetObjectRotationEuler GetObjectRotationEuler;
        public PluginFuncDelegates.P_SetObjectShotReportEnabled SetObjectShotReportEnabled;
        public PluginFuncDelegates.P_IsObjectShotReportEnabled IsObjectShotReportEnabled;
        public PluginFuncDelegates.P_SetObjectTouchedReportEnabled SetObjectTouchedReportEnabled;
        public PluginFuncDelegates.P_IsObjectTouchedReportEnabled IsObjectTouchedReportEnabled;
#pragma warning restore SA1600 // Elements must be documented
    }
}
