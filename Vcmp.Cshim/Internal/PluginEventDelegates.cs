﻿// <copyright file="PluginEventDelegates.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Internal
{
    using System;
    using System.Runtime.InteropServices;
    using Vcmp.Cshim.Enumerations;

    /// <summary>
    /// Delegates used for marshalling events as function pointers
    /// </summary>
    internal unsafe class PluginEventDelegates
    {
        /// <summary>
        /// Called when the server is initialized and ready for plugin calls.
        /// Returning 0 will stop this event from being propagated to other plugins.
        /// </summary>
        /// <returns>1 if the plugin initialized properly, 0 otherwise.</returns>
        public delegate byte P_OnServerInitialise();

        /// <summary>
        /// Called when the server is shutting down.
        /// </summary>
        public delegate void P_OnServerShutdown();

        /// <summary>
        /// Called when the server has performed a tick.
        /// </summary>
        /// <param name="elapsedTime">Time since the last tick in milliseconds</param>
        public delegate void P_OnServerFrame(float elapsedTime);

        /// <summary>
        /// Called when an internal plugin command is issued. This only happens when another
        /// plugin performs a <see cref="PluginFuncDelegates.P_SendPluginCommand">SendPluginCommand</see>
        /// call.
        /// </summary>
        /// <param name="commandIdentifier">A unique identifier for this command</param>
        /// <param name="message">The message associated with the command</param>
        /// <returns>The return value of this event is ignored.</returns>
        public delegate byte P_OnPluginCommand(uint commandIdentifier, [MarshalAs(UnmanagedType.LPStr)] string message);

        /// <summary>
        /// Called when a player is connecting to the server but before being accepted into
        /// the netgame. Returning 0 blocks the player from completing the connection.
        /// </summary>
        /// <param name="playerName">The player's name</param>
        /// <param name="nameBufferSize">The number of bytes in the player name buffer</param>
        /// <param name="userPassword">The password provided by the player, if any</param>
        /// <param name="ipAddress">The player's IP address</param>
        /// <returns>0 if the player should be blocked from connecting, 1 otherwise</returns>
        public delegate byte P_OnIncomingConnection([MarshalAs(UnmanagedType.LPStr)] string playerName, ulong nameBufferSize, [MarshalAs(UnmanagedType.LPStr)] string userPassword, [MarshalAs(UnmanagedType.LPStr)] string ipAddress);

        /// <summary>
        /// Called when receiving data from a client script.
        /// </summary>
        /// <param name="playerId">The ID of the player that triggered the event</param>
        /// <param name="data">The data sent</param>
        /// <param name="size">The number of bytes in the data buffer</param>
        public delegate void P_OnClientScriptData(int playerId, byte* data, ulong size);

        /// <summary>
        /// Called when a player has successfully connected to the server.
        /// </summary>
        /// <param name="playerId">The ID of the player connecting</param>
        public delegate void P_OnPlayerConnect(int playerId);

        /// <summary>
        /// Called when a player has disconnected from the server.
        /// </summary>
        /// <param name="playerId">The ID of the player disconnected</param>
        /// <param name="reason">The reason the player disconnected</param>
        public delegate void P_OnPlayerDisconnect(int playerId, DisconnectReason reason);

        /// <summary>
        /// Called when a player requests a class. Returning 0 prevents the player from
        /// selecting the class on the spawn screen.
        /// </summary>
        /// <param name="playerId">The ID of the player requesting the class</param>
        /// <param name="offset">The offset from 0 of the class</param>
        /// <returns>0 if the player should be blocked from requesting the class, 1 otherwise</returns>
        public delegate byte P_OnPlayerRequestClass(int playerId, int offset);

        /// <summary>
        /// Called when a player requests to spawn. Returning 0 prevents the player from
        /// spawning.
        /// </summary>
        /// <param name="playerId">The ID of the player requesting spawn</param>
        /// <returns>0 if the player should be blocked from spawning, 1 otherwise</returns>
        public delegate byte P_OnPlayerRequestSpawn(int playerId);

        /// <summary>
        /// Called when a player successfully spawns
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        public delegate void P_OnPlayerSpawn(int playerId);

        /// <summary>
        /// Called when a player dies
        /// </summary>
        /// <param name="playerId">The ID of the player who died</param>
        /// <param name="killerId">The ID of the killer, or -1 if no other player was involved</param>
        /// <param name="reason">The reason for death (i.e. the weapon ID)</param>
        /// <param name="bodyPart">The last bodypart to be shot before dying</param>
        public delegate void P_OnPlayerDeath(int playerId, int killerId, int reason, BodyPart bodyPart);

        /// <summary>
        /// Called when a player sends a sync packet.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="updateType">The type of sync packet sent</param>
        /// <remarks>
        /// Performing heavy processing in this call can adversely affect the
        /// performance of the server by blocking the main netgame thread.
        /// </remarks>
        public delegate void P_OnPlayerUpdate(int playerId, PlayerUpdate updateType);

        /// <summary>
        /// Called when a player requests to enter a vehicle. Returning 0 prevents the
        /// player from doing so.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="vehicleId">The vehicle's ID</param>
        /// <param name="slotIndex">The index of the seat the player is trying to enter</param>
        /// <returns>0 if the player should not be allowed to enter, 1 otherwise</returns>
        public delegate byte P_OnPlayerRequestEnterVehicle(int playerId, int vehicleId, int slotIndex);

        /// <summary>
        /// Called when a player successfully enters a vehicle.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="vehicleId">The vehicle's ID</param>
        /// <param name="slotIndex">The index of the seat the player entered</param>
        public delegate void P_OnPlayerEnterVehicle(int playerId, int vehicleId, int slotIndex);

        /// <summary>
        /// Called when a player successfully exits a vehicle.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="vehicleId">The vehicle's ID</param>
        public delegate void P_OnPlayerExitVehicle(int playerId, int vehicleId);

        /// <summary>
        /// Called when a player's name is changed by another plugin call.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="oldName">The player's old name</param>
        /// <param name="newName">The player's new name</param>
        public delegate void P_OnPlayerNameChange(int playerId, [MarshalAs(UnmanagedType.LPStr)] string oldName, [MarshalAs(UnmanagedType.LPStr)] string newName);

        /// <summary>
        /// Called when a player changes state.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="oldState">The player's old state</param>
        /// <param name="newState">The player's new state</param>
        public delegate void P_OnPlayerStateChange(int playerId, PlayerState oldState, PlayerState newState);

        /// <summary>
        /// Called when a player changes actions.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="oldAction">The player's old action</param>
        /// <param name="newAction">The player's new action</param>
        public delegate void P_OnPlayerActionChange(int playerId, int oldAction, int newAction);

        /// <summary>
        /// Called when a player changes on-fire state.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="isOnFire">Whether the player is now on fire or not</param>
        public delegate void P_OnPlayerOnFireChange(int playerId, byte isOnFire);

        /// <summary>
        /// Called when a player changes crouch state.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="isCrouching">Whether the player is now crouching or not</param>
        public delegate void P_OnPlayerCrouchChange(int playerId, byte isCrouching);

        /// <summary>
        /// Called when a player changes the game keys being pressed down.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="oldKeys">The keys that were previously being pressed</param>
        /// <param name="newKeys">The keys that are now being pressed</param>
        public delegate void P_OnPlayerGameKeysChange(int playerId, uint oldKeys, uint newKeys);

        /// <summary>
        /// Called when a player starts typing.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        public delegate void P_OnPlayerBeginTyping(int playerId);

        /// <summary>
        /// Called when a player stops typing.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        public delegate void P_OnPlayerEndTyping(int playerId);

        /// <summary>
        /// Called when a player changes AFK state.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="isAway">Whether the player is now away or not</param>
        public delegate void P_OnPlayerAwayChange(int playerId, byte isAway);

        /// <summary>
        /// Called when a player sends a message to chat. Returning 0 stops
        /// the message from being sent.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="message">The message sent</param>
        /// <returns>0 if the message should not be sent, 1 otherwise</returns>
        public delegate byte P_OnPlayerMessage(int playerId, [MarshalAs(UnmanagedType.LPStr)] string message);

        /// <summary>
        /// Called when a player tries to use a command. Returning 0 indicates that
        /// the command the player tried to use could not be found.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="message">The command text, including the command itself as the first word</param>
        /// <returns>0 if command could not be processed, 1 otherwise</returns>
        public delegate byte P_OnPlayerCommand(int playerId, [MarshalAs(UnmanagedType.LPStr)] string message);

        /// <summary>
        /// Called when a player tries to send a private message. Returning 0 prevents
        /// the message from being sent.
        /// </summary>
        /// <param name="playerId">The sending player's ID</param>
        /// <param name="targetPlayerId">The target player's ID</param>
        /// <param name="message">The message being sent</param>
        /// <returns>0 if the message should not be sent, 1 otherwise</returns>
        public delegate byte P_OnPlayerPrivateMessage(int playerId, int targetPlayerId, [MarshalAs(UnmanagedType.LPStr)] string message);

        /// <summary>
        /// Called when a player triggers a keybind.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="bindId">The keybind ID</param>
        public delegate void P_OnPlayerKeyBindDown(int playerId, int bindId);

        /// <summary>
        /// Called when a player releases a keybind.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="bindId">The keybind ID</param>
        public delegate void P_OnPlayerKeyBindUp(int playerId, int bindId);

        /// <summary>
        /// Called when a player starts spectating another player.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="targetPlayerId">The ID of the spectated player</param>
        public delegate void P_OnPlayerSpectate(int playerId, int targetPlayerId);

        /// <summary>
        /// Called when a player crashes.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="report">The fulltext of the crash report</param>
        public delegate void P_OnPlayerCrashReport(int playerId, [MarshalAs(UnmanagedType.LPStr)] string report);

        /// <summary>
        /// Called when a vehicle update event occurs.
        /// </summary>
        /// <param name="vehicleId">The vehicle's ID</param>
        /// <param name="updateType">The type of update event</param>
        public delegate void P_OnVehicleUpdate(int vehicleId, VehicleUpdate updateType);

        /// <summary>
        /// Called when a vehicle explodes.
        /// </summary>
        /// <param name="vehicleId">The vehicle's ID</param>
        public delegate void P_OnVehicleExplode(int vehicleId);

        /// <summary>
        /// Called when a vehicle respawns.
        /// </summary>
        /// <param name="vehicleId">The vehicle's ID</param>
        public delegate void P_OnVehicleRespawn(int vehicleId);

        /// <summary>
        /// Called when a dynamic object is shot.
        /// </summary>
        /// <param name="objectId">The object's ID</param>
        /// <param name="playerId">The shooter's ID</param>
        /// <param name="weaponId">The weapon the object was shot with</param>
        public delegate void P_OnObjectShot(int objectId, int playerId, int weaponId);

        /// <summary>
        /// Called when a dynamic object is bumped by a player.
        /// </summary>
        /// <param name="objectId">The object's ID</param>
        /// <param name="playerId">The player's ID</param>
        public delegate void P_OnObjectTouched(int objectId, int playerId);

        /// <summary>
        /// Called when a player attempts to use a pickup. Returning 0 prevents
        /// <see cref="P_OnPickupPicked">OnPickupPicked</see> from firing.
        /// </summary>
        /// <param name="pickupId">The pickup's ID</param>
        /// <param name="playerId">The ID of the player trying to use the pickup</param>
        /// <returns>1 if the pickup attempt should succeed, 0 otherwise</returns>
        public delegate byte P_OnPickupPickAttempt(int pickupId, int playerId);

        /// <summary>
        /// Called when a player successfully picks up a pickup.
        /// </summary>
        /// <param name="pickupId">The pickup's ID</param>
        /// <param name="playerId">The player's ID</param>
        public delegate void P_OnPickupPicked(int pickupId, int playerId);

        /// <summary>
        /// Called when a pickup respawns.
        /// </summary>
        /// <param name="pickupId">The pickup's ID</param>
        public delegate void P_OnPickupRespawn(int pickupId);

        /// <summary>
        /// Called when a player enters a checkpoint or sphere.
        /// </summary>
        /// <param name="checkPointId">The checkpoint's ID</param>
        /// <param name="playerId">The player's ID</param>
        public delegate void P_OnCheckpointEntered(int checkPointId, int playerId);

        /// <summary>
        /// Called when a player exits a checkpoint or sphere.
        /// </summary>
        /// <param name="checkPointId">The checkpoint's ID</param>
        /// <param name="playerId">The player's ID</param>
        public delegate void P_OnCheckpointExited(int checkPointId, int playerId);

        /// <summary>
        /// Called when an entity pool change occurs due to a created or destroyed
        /// entity.
        /// </summary>
        /// <param name="entityType">The type of entity</param>
        /// <param name="entityId">The ID of the entity</param>
        /// <param name="isDeleted">Whether the entity is being destroyed or created</param>
        public delegate void P_OnEntityPoolChange(EntityPool entityType, int entityId, byte isDeleted);

        /// <summary>
        /// Called when the server is reporting performance statistics
        /// </summary>
        /// <param name="entryCount">The number of statistics being reported</param>
        /// <param name="descriptions">An array of strings describing each reported statistic</param>
        /// <param name="times">The values for each reported statistic</param>
        public delegate void P_OnServerPerformanceReport(ulong entryCount, IntPtr descriptions, ulong* times);
    }
}
