﻿// <copyright file="PluginFuncDelegates.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Internal
{
    using System;
    using System.Runtime.InteropServices;
    using System.Text;
    using Vcmp.Cshim.Enumerations;
    using Vcmp.Cshim.Models;

    /// <summary>
    /// A collection of delegates (function pointers) for VC:MP plugin
    /// functions.
    /// </summary>
    internal unsafe class PluginFuncDelegates
    {
        public delegate uint P_GetServerVersion();
        public delegate ErrorCode P_GetServerSettings(ref ServerSettings settings);

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_ExportFunctions(int pluginId, void** functionList, ulong size);
        public delegate uint P_GetNumberOfPlugins();
        public delegate ErrorCode P_GetPluginInfo(int pluginId, ref PluginInfo pluginInfo);

        /// <summary>
        /// Gets a plugin ID given a plugin name.
        /// </summary>
        /// <param name="pluginName">The name for the plugin to look up</param>
        /// <returns>-1 if the plugin could not be found. Any other value otherwise.</returns>
        public delegate int P_FindPlugin([MarshalAs(UnmanagedType.LPStr)] string pluginName);

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetPluginExports(int pluginId, void** exports, ref ulong exportCount);

        /// <summary>
        /// Broadcasts a message to all server plugins, directed to the
        /// <see cref="PluginEventDelegates.P_OnPluginCommand"/> event.
        /// </summary>
        /// <param name="commandIdentifier">A <see langword="uint"/> identifying the type of command</param>
        /// <param name="data">Pointer to the bytes to send</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if the format string is <see langword="null"/>.
        /// <see cref="ErrorCode.TooLargeInput"/> if the message is larger than 8192 bytes (8KiB).
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_SendPluginCommand(uint commandIdentifier, IntPtr data);
        public delegate ulong P_GetTime();

        /// <summary>
        /// Prints a message to the server console and logs it in the serverlog.
        /// </summary>
        /// <param name="message">The string for the message</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="message"/> is <see langword="null"/>.
        /// <see cref="ErrorCode.TooLargeInput"/> if the message is larger than 8192 bytes (8KiB).
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_LogMessage([MarshalAs(UnmanagedType.LPStr)] string message);
        public delegate ErrorCode P_GetLastError();

        /// <summary>
        /// Sends data to clients to be processed by client-side scripts.
        /// </summary>
        /// <param name="playerId">The ID of the player to send to, or -1 to send to all players</param>
        /// <param name="data">A pointer to bytes of data to send</param>
        /// <param name="size">The number of bytes to send</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if the data pointer is <see langword="null"/>.
        /// <see cref="ErrorCode.TooLargeInput"/> if the message is too large. (This condition is currently unused.)
        /// <see cref="ErrorCode.NoSuchEntity"/> if the player ID does not match a connected player.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_SendClientScriptData(int playerId, IntPtr data, ulong size);

        /// <summary>
        /// Sends a chat message to clients.
        /// </summary>
        /// <param name="playerId">The ID of the player to send to, or -1 to send to all players</param>
        /// <param name="colour">An <see langword="int"/> representation of an RGBA color for the text</param>
        /// <param name="message">The string for the message</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="message"/> is <see langword="null"/>.
        /// <see cref="ErrorCode.TooLargeInput"/> if the message is larger than 8192 bytes (8KiB).
        /// <see cref="ErrorCode.NoSuchEntity"/> if the player ID does not match a connected player.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_SendClientMessage(int playerId, uint colour, [MarshalAs(UnmanagedType.LPStr)] string message);

        /// <summary>
        /// Sends a game message to clients, e.g. hint text in a black box in
        /// the top left corner.
        /// </summary>
        /// <param name="playerId">The ID of the player to send to, or -1 to send to all players</param>
        /// <param name="type">The type of message to send; see VC:MP wiki for more info</param>
        /// <param name="message">The message to send</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="message"/> is <see langword="null"/>.
        /// <see cref="ErrorCode.TooLargeInput"/> if the message is larger than 8192 bytes (8KiB).
        /// <see cref="ErrorCode.NoSuchEntity"/> if the player ID does not match a connected player.
        /// <see cref="ErrorCode.ArgumentOutOfBounds"/> if the message type is invalid. (This condition is currently unused.)
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_SendGameMessage(int playerId, int type, [MarshalAs(UnmanagedType.LPStr)] string message);

        /// <summary>
        /// Sets the server name.
        /// </summary>
        /// <param name="text">The new server name</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="text"/> is <see langword="null"/>.
        /// <see cref="ErrorCode.TooLargeInput"/> if the name is longer than 128 bytes.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        /// <seealso cref="P_GetServerName"/>
        public delegate ErrorCode P_SetServerName([MarshalAs(UnmanagedType.LPStr)] string text);

        /// <summary>
        /// Gets the server name and stores it in the buffer given.
        /// </summary>
        /// <param name="buffer">The <see cref="StringBuilder"/> to store the server name in.</param>
        /// <param name="size">The size of the buffer in bytes.</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="buffer"/> is <see langword="null"/>.
        /// <see cref="ErrorCode.BufferTooSmall"/> if the buffer is too small to fit the server name.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        /// <seealso cref="P_SetServerName"/>
        public delegate ErrorCode P_GetServerName([MarshalAs(UnmanagedType.LPStr)] StringBuilder buffer, ulong size);

        /// <summary>
        /// Sets the player limit of the server.
        /// </summary>
        /// <param name="maxPlayers">The new player limit</param>
        /// <returns>
        /// <see cref="ErrorCode.ArgumentOutOfBounds"/> if <paramref name="maxPlayers" />
        /// is less than 1 or greater than 100.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        /// <seealso cref="P_GetMaxPlayers"/>
        public delegate ErrorCode P_SetMaxPlayers(uint maxPlayers);

        /// <summary>
        /// Gets the server's current player limit.
        /// </summary>
        /// <returns>The maximum number of players allowed</returns>
        /// <seealso cref="P_SetMaxPlayers"/>
        public delegate uint P_GetMaxPlayers();

        /// <summary>
        /// Sets a password for the server.
        /// </summary>
        /// <param name="password">The new password. Unlocks the server if empty.</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="password"/> is a <see langword="null"/> pointer.
        /// <see cref="ErrorCode.TooLargeInput"/> if the password is longer than 64 characters.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        /// <seealso cref="P_GetServerPassword"/>
        public delegate ErrorCode P_SetServerPassword([MarshalAs(UnmanagedType.LPStr)] string password);

        /// <summary>
        /// Gets the server password and stores it in the buffer given.
        /// </summary>
        /// <param name="buffer">The <see cref="StringBuilder"/> to store the server password in.</param>
        /// <param name="size">The size of the buffer in bytes.</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="buffer"/> is <see langword="null"/>.
        /// <see cref="ErrorCode.BufferTooSmall"/> if the buffer is too small to fit the server password.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_GetServerPassword([MarshalAs(UnmanagedType.LPStr)] StringBuilder buffer, ulong size);

        /// <summary>
        /// Sets a gamemode name for the server.
        /// </summary>
        /// <param name="gameMode">The new gamemode name.</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="gameMode"/> is a <see langword="null"/> pointer.
        /// <see cref="ErrorCode.TooLargeInput"/> if the password is longer than 64 characters.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_SetGameModeText([MarshalAs(UnmanagedType.LPStr)] string gameMode);

        /// <summary>
        /// Gets the gamemode name and stores it in the buffer given.
        /// </summary>
        /// <param name="buffer">The <see cref="StringBuilder"/> to store the gamemode name in.</param>
        /// <param name="size">The size of the buffer in bytes.</param>
        /// <returns>
        /// <see cref="ErrorCode.NullArgument"/> if <paramref name="buffer"/> is <see langword="null"/>.
        /// <see cref="ErrorCode.BufferTooSmall"/> if the buffer is too small to fit the gamemode name.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_GetGameModeText([MarshalAs(UnmanagedType.LPStr)] StringBuilder buffer, ulong size);

        /// <summary>
        /// Gracefully stops the server.
        /// </summary>
        public delegate void P_ShutdownServer();

        /// <summary>
        /// Toggles a boolean setting, like whether wallglitch is enabled.
        /// </summary>
        /// <param name="option">The server flag to toggle</param>
        /// <param name="toggle">0 to disable the flag, 1 to enable it</param>
        /// <returns>
        /// <see cref="ErrorCode.ArgumentOutOfBounds"/> if <paramref name="toggle"/> is not 0 or 1.
        /// <see cref="ErrorCode.None"/> on success.
        /// </returns>
        public delegate ErrorCode P_SetServerOption(ServerOption option, byte toggle);

        /// <summary>
        /// Gets whether or not a boolean setting is currently enabled.
        /// </summary>
        /// <param name="option">The option to check the status of</param>
        /// <returns>0 if the setting is disabled, 1 otherwise</returns>
        public delegate byte P_GetServerOption(ServerOption option);

        /// <summary>
        /// Sets the boundaries of the play area for all players and worlds.
        /// </summary>
        /// <param name="maxX">The X-coordinate of the northeast boundary</param>
        /// <param name="minX">The X-coordinate of the southwest boundary</param>
        /// <param name="maxY">The Y-coordinate of the northeast boundary</param>
        /// <param name="minY">The Y-coordinate of the southwest boundary</param>
        public delegate void P_SetWorldBounds(float maxX, float minX, float maxY, float minY);

        /// <summary>
        /// Gets the boundaries of the play area for all players and worlds.
        /// </summary>
        /// <param name="maxXOut">Reference for the X-coordinate of the northeast boundary</param>
        /// <param name="minXOut">Reference for the X-coordinate of the southwest boundary</param>
        /// <param name="maxYOut">Reference for the Y-coordinate of the northeast boundary</param>
        /// <param name="minYOut">Reference for the Y-coordinate of the southwest boundary</param>
        public delegate void P_GetWorldBounds(ref float maxXOut, ref float minXOut, ref float maxYOut, ref float minYOut);

        /* success */
        public delegate void P_SetWastedSettings(uint deathTimer, uint fadeTimer, float fadeInSpeed, float fadeOutSpeed, uint fadeColour, uint corpseFadeStart, uint corpseFadeTime);

        /* success */
        public delegate void P_GetWastedSettings(ref uint deathTimerOut, ref uint fadeTimerOut, ref float fadeInSpeedOut, ref float fadeOutSpeedOut, ref uint fadeColourOut, ref uint corpseFadeStartOut, ref uint corpseFadeTimeOut);

        /* success */
        public delegate void P_SetTimeRate(int timeRate);

        /* success */
        public delegate int P_GetTimeRate();

        /* success */
        public delegate void P_SetHour(int hour);

        /* success */
        public delegate int P_GetHour();

        /* success */
        public delegate void P_SetMinute(int minute);

        /* success */
        public delegate int P_GetMinute();

        /* success */
        public delegate void P_SetWeather(int weather);

        /* success */
        public delegate int P_GetWeather();

        /* success */
        public delegate void P_SetGravity(float gravity);

        /* success */
        public delegate float P_GetGravity();

        /* success */
        public delegate void P_SetGameSpeed(float gameSpeed);

        /* success */
        public delegate float P_GetGameSpeed();

        /* success */
        public delegate void P_SetWaterLevel(float waterLevel);

        /* success */
        public delegate float P_GetWaterLevel();

        /* success */
        public delegate void P_SetMaximumFlightAltitude(float height);

        /* success */
        public delegate float P_GetMaximumFlightAltitude();

        /* success */
        public delegate void P_SetKillCommandDelay(int delay);

        /* success */
        public delegate int P_GetKillCommandDelay();

        /* success */
        public delegate void P_SetVehiclesForcedRespawnHeight(float height);

        /* success */
        public delegate float P_GetVehiclesForcedRespawnHeight();

        /*
         * Miscellaneous things
         */

        /* ErrorCodeArgumentOutOfBounds, ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_CreateExplosion(int worldId, int type, float x, float y, float z, int responsiblePlayerId, byte atGroundLevel);
        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_PlaySound(int worldId, int soundId, float x, float y, float z);
        /* success */
        public delegate void P_HideMapObject(int modelId, short tenthX, short tenthY, short tenthZ);
        /* success */
        public delegate void P_ShowMapObject(int modelId, short tenthX, short tenthY, short tenthZ);
        /* success */
        public delegate void P_ShowAllMapObjects();

        /*
         * Weapon settings
         */

        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetWeaponDataValue(int weaponId, int fieldId, double value);
        /* GetLastError: ErrorCodeArgumentOutOfBounds */
        public delegate double P_GetWeaponDataValue(int weaponId, int fieldId);
        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_ResetWeaponDataValue(int weaponId, int fieldId);
        /* GetLastError: ErrorCodeArgumentOutOfBounds */
        public delegate byte P_IsWeaponDataValueModified(int weaponId, int fieldId);
        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_ResetWeaponData(int weaponId);
        /* success */
        public delegate void P_ResetAllWeaponData();

        /*
         * Key binds
         */

        /* -1 == EntityNone */
        public delegate int P_GetKeyBindUnusedSlot();
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetKeyBindData(int bindId, ref byte isCalledOnReleaseOut, ref int keyOneOut, ref int keyTwoOut, ref int keyThreeOut);
        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_RegisterKeyBind(int bindId, byte isCalledOnRelease, int keyOne, int keyTwo, int keyThree);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RemoveKeyBind(int bindId);
        /* success */
        public delegate void P_RemoveAllKeyBinds();

        /*
         * Coordinate blips
         */

        /* GetLastError: ErrorCodePoolExhausted */
        public delegate int P_CreateCoordBlip(int index, int world, float x, float y, float z, int scale, uint colour, int sprite);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_DestroyCoordBlip(int index);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetCoordBlipInfo(int index, ref int worldOut, ref float xOut, ref float yOut, ref float zOut, ref int scaleOut, ref uint colourOut, ref int spriteOut);

        /*
         * Radios
         */

        /* ErrorCodeArgumentOutOfBounds, ErrorCodeNullArgument */
        public delegate ErrorCode P_AddRadioStream(int radioId, [MarshalAs(UnmanagedType.LPStr)] string radioName, [MarshalAs(UnmanagedType.LPStr)] string radioUrl, byte isListed);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RemoveRadioStream(int radioId);

        /*
         * Spawning and classes
         */

        /* GetLastError: ErrorCodeArgumentOutOfBounds, ErrorCodePoolExhausted */
        public delegate int P_AddPlayerClass(int teamId, uint colour, int modelIndex, float x, float y, float z, float angle, int weaponOne, int weaponOneAmmo, int weaponTwo, int weaponTwoAmmo, int weaponThree, int weaponThreeAmmo);
        /* success */
        public delegate void P_SetSpawnPlayerPosition(float x, float y, float z);
        /* success */
        public delegate void P_SetSpawnCameraPosition(float x, float y, float z);
        /* success */
        public delegate void P_SetSpawnCameraLookAt(float x, float y, float z);

        /*
         * Administration
         */

        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerAdmin(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerAdmin(int playerId, byte toggle);
        /* ErrorCodeNoSuchEntity, ErrorCodeNullArgument, ErrorCodeBufferTooSmall */
        public delegate ErrorCode P_GetPlayerIP(int playerId, [MarshalAs(UnmanagedType.LPStr)] StringBuilder buffer, ulong size);
        /* ErrorCodeNoSuchEntity, ErrorCodeNullArgument, ErrorCodeBufferTooSmall */
        public delegate ErrorCode P_GetPlayerUID(int playerId, [MarshalAs(UnmanagedType.LPStr)] StringBuilder buffer, ulong size);
        /* ErrorCodeNoSuchEntity, ErrorCodeNullArgument, ErrorCodeBufferTooSmall */
        public delegate ErrorCode P_GetPlayerUID2(int playerId, [MarshalAs(UnmanagedType.LPStr)] StringBuilder buffer, ulong size);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_KickPlayer(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_BanPlayer(int playerId);
        /* success */
        public delegate void P_BanIP([MarshalAs(UnmanagedType.LPStr)] string ipAddress);
        /* success */
        public delegate byte P_UnbanIP([MarshalAs(UnmanagedType.LPStr)] string ipAddress);
        /* success */
        public delegate byte P_IsIPBanned([MarshalAs(UnmanagedType.LPStr)] string ipAddress);

        /*
         * Player access and basic info
         */

        /* -1 == EntityNone */
        public delegate int P_GetPlayerIdFromName([MarshalAs(UnmanagedType.LPStr)] string name);
        /* success */
        public delegate byte P_IsPlayerConnected(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerStreamedForPlayer(int checkedPlayerId, int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate uint P_GetPlayerKey(int playerId);
        /* ErrorCodeNoSuchEntity, ErrorCodeNullArgument, ErrorCodeBufferTooSmall */
        public delegate ErrorCode P_GetPlayerName(int playerId, [MarshalAs(UnmanagedType.LPStr)] StringBuilder buffer, ulong size);
        /* ErrorCodeNoSuchEntity, ErrorCodeNullArgument, ErrorCodeInvalidName, ErrorCodeTooLargeInput */
        public delegate ErrorCode P_SetPlayerName(int playerId, [MarshalAs(UnmanagedType.LPStr)] string name);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate PlayerState P_GetPlayerState(int playerId);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetPlayerOption(int playerId, PlayerOption option, byte toggle);
        /* GetLastError: ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate byte P_GetPlayerOption(int playerId, PlayerOption option);

        /*
         * Player world
         */

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerWorld(int playerId, int world);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerWorld(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerSecondaryWorld(int playerId, int secondaryWorld);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerSecondaryWorld(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerUniqueWorld(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerWorldCompatible(int playerId, int world);

        /*
         * Player class, team, skin, colour
         */

        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerClass(int playerId);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetPlayerTeam(int playerId, int teamId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerTeam(int playerId);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetPlayerSkin(int playerId, int skinId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerSkin(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerColour(int playerId, uint colour);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate uint P_GetPlayerColour(int playerId);

        /*
         * Player spawn cycle
         */

        /* ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerSpawned(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_ForcePlayerSpawn(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_ForcePlayerSelect(int playerId);
        /* success */
        public delegate void P_ForceAllSelect();
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerTyping(int playerId);

        /*
         * Player money, score, wanted level
         */

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GivePlayerMoney(int playerId, int amount);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerMoney(int playerId, int amount);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerMoney(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerScore(int playerId, int score);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerScore(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerWantedLevel(int playerId, int level);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerWantedLevel(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerPing(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate double P_GetPlayerFPS(int playerId);

        /*
         * Player health and immunity
         */

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerHealth(int playerId, float health);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate float P_GetPlayerHealth(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerArmour(int playerId, float armour);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate float P_GetPlayerArmour(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerImmunityFlags(int playerId, uint flags);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate uint P_GetPlayerImmunityFlags(int playerId);

        /*
         * Player position and rotation
         */

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerPosition(int playerId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetPlayerPosition(int playerId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerSpeed(int playerId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetPlayerSpeed(int playerId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_AddPlayerSpeed(int playerId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerHeading(int playerId, float angle);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate float P_GetPlayerHeading(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerAlpha(int playerId, int alpha, uint fadeTime);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerAlpha(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetPlayerAimPosition(int playerId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetPlayerAimDirection(int playerId, ref float xOut, ref float yOut, ref float zOut);

        /*
         * Player actions and keys
         */

        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerOnFire(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerCrouching(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerAction(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate uint P_GetPlayerGameKeys(int playerId);

        /*
         * Player vehicle
         */

        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds, ErrorCodeRequestDenied */
        public delegate ErrorCode P_PutPlayerInVehicle(int playerId, int vehicleId, int slotIndex, byte makeRoom, byte warp);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RemovePlayerFromVehicle(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate PlayerVehicle P_GetPlayerInVehicleStatus(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerInVehicleSlot(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerVehicleId(int playerId);

        /*
         * Player weapons
         */

        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_GivePlayerWeapon(int playerId, int weaponId, int ammo);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetPlayerWeapon(int playerId, int weaponId, int ammo);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerWeapon(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerWeaponAmmo(int playerId);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetPlayerWeaponSlot(int playerId, int slot);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerWeaponSlot(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate int P_GetPlayerWeaponAtSlot(int playerId, int slot);
        /* GetLastError: ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate int P_GetPlayerAmmoAtSlot(int playerId, int slot);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RemovePlayerWeapon(int playerId, int weaponId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RemoveAllWeapons(int playerId);

        /*
         * Player camera
         */

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetCameraPosition(int playerId, float posX, float posY, float posZ, float lookX, float lookY, float lookZ);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RestoreCamera(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsCameraLocked(int playerId);

        /*
         * Player miscellaneous stuff
         */

        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerAnimation(int playerId, int groupId, int animationId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerStandingOnVehicle(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerStandingOnObject(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPlayerAway(int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate int P_GetPlayerSpectateTarget(int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPlayerSpectateTarget(int playerId, int targetId);
        /* ErrorCodeNoSuchEntity, ErrorCodeNullArgument */
        public delegate ErrorCode P_RedirectPlayerToServer(int playerId, [MarshalAs(UnmanagedType.LPStr)] string ip, uint port, [MarshalAs(UnmanagedType.LPStr)] string nick, [MarshalAs(UnmanagedType.LPStr)] string serverPassword, [MarshalAs(UnmanagedType.LPStr)] string userPassword);

        /*
         * All entities
         */

        /* GetLastError: vcmpArgumentOutOfBounds */
        public delegate byte P_CheckEntityExists(EntityPool entityPool, int index);

        /*
         * Vehicles
         */

        /* GetLastError: ErrorCodeArgumentOutOfBounds, ErrorCodePoolExhausted */
        public delegate int P_CreateVehicle(int modelIndex, int world, float x, float y, float z, float angle, int primaryColour, int secondaryColour);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_DeleteVehicle(int vehicleId);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetVehicleOption(int vehicleId, VehicleOption option, byte toggle);
        /* GetLastError: ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate byte P_GetVehicleOption(int vehicleId, VehicleOption option);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetVehicleSyncSource(int vehicleId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate VehicleSync P_GetVehicleSyncType(int vehicleId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsVehicleStreamedForPlayer(int vehicleId, int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleWorld(int vehicleId, int world);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetVehicleWorld(int vehicleId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetVehicleModel(int vehicleId);
        /* GetLastError: ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate int P_GetVehicleOccupant(int vehicleId, int slotIndex);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RespawnVehicle(int vehicleId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleImmunityFlags(int vehicleId, uint immunityFlags);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate uint P_GetVehicleImmunityFlags(int vehicleId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_ExplodeVehicle(int vehicleId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsVehicleWrecked(int vehicleId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehiclePosition(int vehicleId, float x, float y, float z, byte removeOccupants);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehiclePosition(int vehicleId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleRotation(int vehicleId, float x, float y, float z, float w);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleRotationEuler(int vehicleId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleRotation(int vehicleId, ref float xOut, ref float yOut, ref float zOut, ref float wOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleRotationEuler(int vehicleId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleSpeed(int vehicleId, float x, float y, float z, byte add, byte relative);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleSpeed(int vehicleId, ref float xOut, ref float yOut, ref float zOut, byte relative);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleTurnSpeed(int vehicleId, float x, float y, float z, byte add, byte relative);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleTurnSpeed(int vehicleId, ref float xOut, ref float yOut, ref float zOut, byte relative);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleSpawnPosition(int vehicleId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleSpawnPosition(int vehicleId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleSpawnRotation(int vehicleId, float x, float y, float z, float w);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleSpawnRotationEuler(int vehicleId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleSpawnRotation(int vehicleId, ref float xOut, ref float yOut, ref float zOut, ref float wOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleSpawnRotationEuler(int vehicleId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleIdleRespawnTimer(int vehicleId, uint millis);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate uint P_GetVehicleIdleRespawnTimer(int vehicleId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleHealth(int vehicleId, float health);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate float P_GetVehicleHealth(int vehicleId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleColour(int vehicleId, int primaryColour, int secondaryColour);
        /* ErrorCodeNoSuchEntity, ErrorCodeNullArgument */
        public delegate ErrorCode P_GetVehicleColour(int vehicleId, ref int primaryColourOut, ref int secondaryColourOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehiclePartStatus(int vehicleId, int partId, int status);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetVehiclePartStatus(int vehicleId, int partId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleTyreStatus(int vehicleId, int tyreId, int status);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetVehicleTyreStatus(int vehicleId, int tyreId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleDamageData(int vehicleId, uint damageData);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate uint P_GetVehicleDamageData(int vehicleId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetVehicleRadio(int vehicleId, int radioId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetVehicleRadio(int vehicleId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetVehicleTurretRotation(int vehicleId, ref float horizontalOut, ref float verticalOut);

        /*
         * Vehicle handling
         */

        /* success */
        public delegate void P_ResetAllVehicleHandlings();
        /* ErrorCodeArgumentOutOfBounds */
        public delegate byte P_ExistsHandlingRule(int modelIndex, int ruleIndex);
        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetHandlingRule(int modelIndex, int ruleIndex, double value);
        /* GetLastError: ErrorCodeArgumentOutOfBounds */
        public delegate double P_GetHandlingRule(int modelIndex, int ruleIndex);
        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_ResetHandlingRule(int modelIndex, int ruleIndex);
        /* ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_ResetHandling(int modelIndex);
        /* GetLastError: ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate byte P_ExistsInstHandlingRule(int vehicleId, int ruleIndex);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_SetInstHandlingRule(int vehicleId, int ruleIndex, double value);
        /* GetLastError: ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate double P_GetInstHandlingRule(int vehicleId, int ruleIndex);
        /* ErrorCodeNoSuchEntity, ErrorCodeArgumentOutOfBounds */
        public delegate ErrorCode P_ResetInstHandlingRule(int vehicleId, int ruleIndex);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_ResetInstHandling(int vehicleId);

        /*
         * Pickups
         */

        /* ErrorCodePoolExhausted */
        public delegate int P_CreatePickup(int modelIndex, int world, int quantity, float x, float y, float z, int alpha, byte isAutomatic);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_DeletePickup(int pickupId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPickupStreamedForPlayer(int pickupId, int playerId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPickupWorld(int pickupId, int world);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPickupWorld(int pickupId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPickupAlpha(int pickupId, int alpha);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPickupAlpha(int pickupId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPickupIsAutomatic(int pickupId, byte toggle);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsPickupAutomatic(int pickupId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPickupAutoTimer(int pickupId, uint durationMillis);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate uint P_GetPickupAutoTimer(int pickupId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RefreshPickup(int pickupId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetPickupPosition(int pickupId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetPickupPosition(int pickupId, ref float xOut, ref float yOut, ref float zOut);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPickupModel(int pickupId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetPickupQuantity(int pickupId);

        /*
         * Checkpoints
         */

        /* ErrorCodePoolExhausted, ErrorCodeNoSuchEntity */
        public delegate int P_CreateCheckPoint(int playerId, int world, byte isSphere, float x, float y, float z, int red, int green, int blue, int alpha, float radius);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_DeleteCheckPoint(int checkPointId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsCheckPointStreamedForPlayer(int checkPointId, int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsCheckPointSphere(int checkPointId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetCheckPointWorld(int checkPointId, int world);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetCheckPointWorld(int checkPointId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetCheckPointColour(int checkPointId, int red, int green, int blue, int alpha);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetCheckPointColour(int checkPointId, ref int redOut, ref int greenOut, ref int blueOut, ref int alphaOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetCheckPointPosition(int checkPointId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetCheckPointPosition(int checkPointId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetCheckPointRadius(int checkPointId, float radius);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate float P_GetCheckPointRadius(int checkPointId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetCheckPointOwner(int checkPointId);

        /*
         * Objects
         */

        /* GetLastError: ErrorCodePoolExhausted */
        public delegate int P_CreateObject(int modelIndex, int world, float x, float y, float z, int alpha);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_DeleteObject(int objectId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsObjectStreamedForPlayer(int objectId, int playerId);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetObjectModel(int objectId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetObjectWorld(int objectId, int world);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetObjectWorld(int objectId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetObjectAlpha(int objectId, int alpha, uint duration);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate int P_GetObjectAlpha(int objectId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_MoveObjectTo(int objectId, float x, float y, float z, uint duration);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_MoveObjectBy(int objectId, float x, float y, float z, uint duration);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetObjectPosition(int objectId, float x, float y, float z);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetObjectPosition(int objectId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RotateObjectTo(int objectId, float x, float y, float z, float w, uint duration);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RotateObjectToEuler(int objectId, float x, float y, float z, uint duration);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RotateObjectBy(int objectId, float x, float y, float z, float w, uint duration);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_RotateObjectByEuler(int objectId, float x, float y, float z, uint duration);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetObjectRotation(int objectId, ref float xOut, ref float yOut, ref float zOut, ref float wOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_GetObjectRotationEuler(int objectId, ref float xOut, ref float yOut, ref float zOut);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetObjectShotReportEnabled(int objectId, byte toggle);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsObjectShotReportEnabled(int objectId);
        /* ErrorCodeNoSuchEntity */
        public delegate ErrorCode P_SetObjectTouchedReportEnabled(int objectId, byte toggle);
        /* GetLastError: ErrorCodeNoSuchEntity */
        public delegate byte P_IsObjectTouchedReportEnabled(int objectId);
    }
}
