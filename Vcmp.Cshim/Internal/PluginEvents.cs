﻿// <copyright file="PluginEvents.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Internal
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// A struct, equivalent to the PluginCallbacksstruct in the server SDK,
    /// containing all the function pointers for this plugin's event
    /// callbacks. Used in <see cref="Core.Director"/> for assigning the
    /// C# shim's callbacks.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    internal struct PluginEvents
    {
        /// <summary>
        /// The size of the struct, in bytes.
        /// </summary>
        public uint StructSize;

#pragma warning disable SA1600 // Elements must be documented
        public PluginEventDelegates.P_OnServerInitialise OnServerInitialise;
        public PluginEventDelegates.P_OnServerShutdown OnServerShutdown;
        public PluginEventDelegates.P_OnServerFrame OnServerFrame;
        public PluginEventDelegates.P_OnPluginCommand OnPluginCommand;

        public PluginEventDelegates.P_OnIncomingConnection OnIncomingConnection;
        public PluginEventDelegates.P_OnClientScriptData OnClientScriptData;

        public PluginEventDelegates.P_OnPlayerConnect OnPlayerConnect;
        public PluginEventDelegates.P_OnPlayerDisconnect OnPlayerDisconnect;
        public PluginEventDelegates.P_OnPlayerRequestClass OnPlayerRequestClass;
        public PluginEventDelegates.P_OnPlayerRequestSpawn OnPlayerRequestSpawn;
        public PluginEventDelegates.P_OnPlayerSpawn OnPlayerSpawn;
        public PluginEventDelegates.P_OnPlayerDeath OnPlayerDeath;
        public PluginEventDelegates.P_OnPlayerUpdate OnPlayerUpdate;

        public PluginEventDelegates.P_OnPlayerRequestEnterVehicle OnPlayerRequestEnterVehicle;
        public PluginEventDelegates.P_OnPlayerEnterVehicle OnPlayerEnterVehicle;
        public PluginEventDelegates.P_OnPlayerExitVehicle OnPlayerExitVehicle;

        public PluginEventDelegates.P_OnPlayerNameChange OnPlayerNameChange;
        public PluginEventDelegates.P_OnPlayerStateChange OnPlayerStateChange;
        public PluginEventDelegates.P_OnPlayerActionChange OnPlayerActionChange;
        public PluginEventDelegates.P_OnPlayerOnFireChange OnPlayerOnFireChange;
        public PluginEventDelegates.P_OnPlayerCrouchChange OnPlayerCrouchChange;
        public PluginEventDelegates.P_OnPlayerGameKeysChange OnPlayerGameKeysChange;
        public PluginEventDelegates.P_OnPlayerBeginTyping OnPlayerBeginTyping;
        public PluginEventDelegates.P_OnPlayerEndTyping OnPlayerEndTyping;
        public PluginEventDelegates.P_OnPlayerAwayChange OnPlayerAwayChange;

        public PluginEventDelegates.P_OnPlayerMessage OnPlayerMessage;
        public PluginEventDelegates.P_OnPlayerCommand OnPlayerCommand;
        public PluginEventDelegates.P_OnPlayerPrivateMessage OnPlayerPrivateMessage;

        public PluginEventDelegates.P_OnPlayerKeyBindDown OnPlayerKeyBindDown;
        public PluginEventDelegates.P_OnPlayerKeyBindUp OnPlayerKeyBindUp;
        public PluginEventDelegates.P_OnPlayerSpectate OnPlayerSpectate;
        public PluginEventDelegates.P_OnPlayerCrashReport OnPlayerCrashReport;

        public PluginEventDelegates.P_OnVehicleUpdate OnVehicleUpdate;
        public PluginEventDelegates.P_OnVehicleExplode OnVehicleExplode;
        public PluginEventDelegates.P_OnVehicleRespawn OnVehicleRespawn;

        public PluginEventDelegates.P_OnObjectShot OnObjectShot;
        public PluginEventDelegates.P_OnObjectTouched OnObjectTouched;

        public PluginEventDelegates.P_OnPickupPickAttempt OnPickupPickAttempt;
        public PluginEventDelegates.P_OnPickupPicked OnPickupPicked;
        public PluginEventDelegates.P_OnPickupRespawn OnPickupRespawn;

        public PluginEventDelegates.P_OnCheckpointEntered OnCheckpointEntered;
        public PluginEventDelegates.P_OnCheckpointExited OnCheckpointExited;

        public PluginEventDelegates.P_OnEntityPoolChange OnEntityPoolChange;
        public PluginEventDelegates.P_OnServerPerformanceReport OnServerPerformanceReport;
#pragma warning restore SA1600 // Elements must be documented
    }
}
