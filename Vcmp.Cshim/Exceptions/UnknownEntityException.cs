﻿// <copyright file="UnknownEntityException.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

using System;
using System.Collections.Generic;
using System.Text;

namespace Vcmp.Cshim.Exceptions
{
    public class UnknownEntityException : Exception
    {
        public UnknownEntityException(string message)
            : base(message)
        {
        }

        public UnknownEntityException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
