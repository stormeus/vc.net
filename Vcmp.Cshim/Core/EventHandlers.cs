﻿// <copyright file="EventHandlers.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Core
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Text;
    using Vcmp.Cshim.Api;
    using Vcmp.Cshim.Events;
    using Vcmp.Cshim.Models;

    public class EventHandlers
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventHandlers"/> class.
        /// </summary>
        /// <param name="director">The director instance this handler is attached to</param>
        public EventHandlers(Director director) => this.Director = director;

        public delegate void ServerInitEventHandler(object sender, CancellableEventArgs e);
        public delegate void ServerShutdownEventHandler(object sender, EventArgs e);
        public delegate void ServerFrameEventHandler(object sender, ServerFrameEventArgs e);
        public delegate void PluginCommandEventHandler(object sender, PluginCommandEventArgs e);
        public delegate void IncomingConnectionEventHandler(object sender, IncomingConnectionEventArgs e);
        public delegate void ClientScriptDataEventHandler(object sender, ClientScriptDataEventArgs e);

        public event ServerInitEventHandler ServerInit;
        public event ServerShutdownEventHandler ServerShutdown;
        public event ServerFrameEventHandler ServerFrame;
        public event PluginCommandEventHandler PluginCommand;
        public event IncomingConnectionEventHandler IncomingConnection;
        public event ClientScriptDataEventHandler ClientScriptData;

        public Director Director { get; private set; }

        internal byte OnServerInitialise()
        {
            var e = new CancellableEventArgs();
            if (this.ServerInit != null)
            {
                this.ServerInit(this, e);
            }

            Console.WriteLine("inited!");
            VcmpServer.Functions.SetServerName("This is a C# server!");

            return (byte)(!e.Cancelled ? 1 : 0);
        }

        internal void OnServerShutdown()
        {
            var e = new EventArgs();
            if (this.ServerShutdown != null)
            {
                this.ServerShutdown(this, e);
            }
        }

        internal void OnServerFrame(float timeElapsed)
        {
            var e = new ServerFrameEventArgs(timeElapsed);
            if (this.ServerFrame != null)
            {
                this.ServerFrame(this, e);
            }
        }

        internal byte OnPluginCommand(uint commandId, string message)
        {
            var e = new PluginCommandEventArgs(commandId, message);
            if (this.PluginCommand != null)
            {
                this.PluginCommand(this, e);
            }

            return 1;
        }

        internal byte OnIncomingConnection(
            string playerName,
            ulong nameBufferSize,
            string userPassword,
            string ipAddress)
        {
            var parsedIp = IPAddress.Parse(ipAddress);
            var e = new IncomingConnectionEventArgs(playerName, userPassword, parsedIp);
            if (this.IncomingConnection != null)
            {
                this.IncomingConnection(this, e);
            }

            return (byte)(!e.Cancelled ? 1 : 0);
        }
    }
}
