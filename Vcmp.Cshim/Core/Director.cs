﻿// <copyright file="Director.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Core
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Loader;
    using Vcmp.Cshim.Internal;
    using Vcmp.Cshim.Models;

    public class Director
    {
        private Dictionary<int, Checkpoint> checkpointPool;
        private Dictionary<int, MapObject> objectPool;
        private Dictionary<int, Pickup> pickupPool;
        private Dictionary<int, Player> playerPool;
        private Dictionary<int, Vehicle> vehiclePool;

        private PluginEventDelegates.P_OnServerInitialise deleServerInit;
        private PluginEventDelegates.P_OnServerShutdown deleServerShutdown;
        private PluginEventDelegates.P_OnServerFrame deleServerFrame;
        private PluginEventDelegates.P_OnPluginCommand delePluginCommand;
        private PluginEventDelegates.P_OnIncomingConnection deleIncomingConnection;

        /// <summary>
        /// Initializes a new instance of the <see cref="Director"/> class.
        /// </summary>
        /// <param name="functions">The server function interface</param>
        /// <param name="events">The server event interface</param>
        internal Director(PluginFuncs functions, ref PluginEvents events)
        {
            this.InternalFunctions = functions;
            this.Events = new EventHandlers(this);

            this.checkpointPool = new Dictionary<int, Checkpoint>();
            this.objectPool = new Dictionary<int, MapObject>();
            this.pickupPool = new Dictionary<int, Pickup>();
            this.playerPool = new Dictionary<int, Player>();
            this.vehiclePool = new Dictionary<int, Vehicle>();

            // We need to bind our delegate entry points to private variables
            // in this class. If we don't, the thunk that marshals unmanaged
            // calls to these managed functions will be garbage collected
            // since there would be no references to the function itself.
            //
            // TL;DR This is magic and you should not touch it.
            this.deleServerInit = this.Events.OnServerInitialise;
            this.deleServerShutdown = this.Events.OnServerShutdown;
            this.deleServerFrame = this.Events.OnServerFrame;
            this.delePluginCommand = this.Events.OnPluginCommand;
            this.deleIncomingConnection = this.Events.OnIncomingConnection;

            // Use the delegate references we stored earlier for the
            // actual marshaling.
            events.OnServerInitialise = this.deleServerInit;
            events.OnServerShutdown = this.deleServerShutdown;
            events.OnServerFrame = this.deleServerFrame;
            events.OnPluginCommand = this.delePluginCommand;
            events.OnIncomingConnection = this.deleIncomingConnection;

            var assemblyPath = Directory.GetCurrentDirectory();
            var pluginsPath = Path.Combine(assemblyPath, "plugins");
            Console.WriteLine(assemblyPath);
            Console.WriteLine(pluginsPath);

            var dllsFound = Directory.GetFiles(pluginsPath, "*.VcmpPlugin.dll", SearchOption.AllDirectories);
            foreach (string dll in dllsFound)
            {
                Console.WriteLine("Trying to load " + dll);

                var assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(dll);
                var method = assembly.GetTypes()
                                     .SelectMany(t => t.GetMethods())
                                     .Where(m => m.GetCustomAttributes(
                                         typeof(Vcmp.Cshim.Api.EntryPoint), false).Count() > 0)
                                     .First();

                if (method != null)
                {
                    Console.WriteLine("Invoking method");
                    method.Invoke(null, new object[] { this });
                }
            }
        }

        /// <summary>
        /// Gets the function interface for the server
        /// </summary>
        internal PluginFuncs InternalFunctions { get; private set; }

        /// <summary>
        /// Gets the event interface
        /// </summary>
        public EventHandlers Events { get; private set; }
    }
}
