﻿// <copyright file="CancellableEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using System;

    public class CancellableEventArgs : EventArgs
    {
        public bool Cancelled
        {
            get;
            private set;
        }

        public void Cancel()
        {
            this.Cancelled = true;
        }
    }
}
