﻿// <copyright file="PlayerDisconnectEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using Vcmp.Cshim.Enumerations;

    /// <summary>
    /// Event arguments for OnPlayerDisconnect
    /// </summary>
    public class PlayerDisconnectEventArgs : PlayerEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerDisconnectEventArgs"/> class.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="reason">The reason for disconnect</param>
        public PlayerDisconnectEventArgs(int playerId, DisconnectReason reason)
            : base(playerId) => this.Reason = reason;

        /// <summary>
        /// Gets the reason the player disconnected
        /// </summary>
        public DisconnectReason Reason { get; private set; }
    }
}
