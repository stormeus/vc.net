﻿// <copyright file="IncomingConnectionEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using System;
    using System.Net;

    /// <summary>
    /// Event arguments for incoming connection requests
    /// </summary>
    public class IncomingConnectionEventArgs : CancellableEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IncomingConnectionEventArgs"/> class.
        /// </summary>
        /// <param name="playerName">The player's nickname</param>
        /// <param name="userPassword">The password given by the player</param>
        /// <param name="ipAddress">The player's IP address</param>
        public IncomingConnectionEventArgs(string playerName, string userPassword, IPAddress ipAddress)
        {
            this.PlayerName = playerName;
            this.Password = userPassword;
            this.IpAddress = ipAddress;
        }

        /// <summary>
        /// Gets the name of the connecting player
        /// </summary>
        public string PlayerName { get; private set; }

        /// <summary>
        /// Gets the password given by the player
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Gets the player's IP address
        /// </summary>
        public IPAddress IpAddress { get; private set; }
    }
}
