﻿// <copyright file="ServerFrameEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using System;

    /// <summary>
    /// Event arguments for OnServerFrame
    /// </summary>
    public class ServerFrameEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServerFrameEventArgs"/> class.
        /// </summary>
        /// <param name="timeElapsed">Time elapsed since last frame</param>
        public ServerFrameEventArgs(float timeElapsed) => this.TimeElapsed = timeElapsed;

        /// <summary>
        /// Gets time elapsed in milliseconds since last frame
        /// </summary>
        public float TimeElapsed { get; private set; }
    }
}
