﻿// <copyright file="PluginCommandEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using System;

    /// <summary>
    /// Event arguments for OnPluginCommand
    /// </summary>
    public class PluginCommandEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PluginCommandEventArgs"/> class.
        /// </summary>
        /// <param name="commandId">The unique ID for the command</param>
        /// <param name="message">The message for the command</param>
        public PluginCommandEventArgs(uint commandId, string message)
        {
            this.CommandId = commandId;
            this.Message = message;
        }

        /// <summary>
        /// Gets the unique ID for this command
        /// </summary>
        public uint CommandId { get; private set; }

        /// <summary>
        /// Gets the message for this command
        /// </summary>
        public string Message { get; private set; }
    }
}
