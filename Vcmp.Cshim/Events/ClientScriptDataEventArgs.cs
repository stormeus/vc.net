﻿// <copyright file="ClientScriptDataEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using System;

    /// <summary>
    /// Event arguments for OnClientScriptData
    /// </summary>
    public class ClientScriptDataEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientScriptDataEventArgs"/> class.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="data">The client script data</param>
        public ClientScriptDataEventArgs(int playerId, byte[] data)
            : base()
        {
            this.PlayerId = playerId;
            this.Data = data;
        }

        /// <summary>
        /// Gets the player's ID
        /// </summary>
        public int PlayerId { get; private set; }

        /// <summary>
        /// Gets the data sent by the client script
        /// </summary>
        public byte[] Data { get; private set; }
    }
}
