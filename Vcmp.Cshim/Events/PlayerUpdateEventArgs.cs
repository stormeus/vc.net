﻿// <copyright file="PlayerUpdateEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using Vcmp.Cshim.Enumerations;

    /// <summary>
    /// Event arguments for OnPlayerUpdate
    /// </summary>
    public class PlayerUpdateEventArgs : PlayerEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerUpdateEventArgs"/> class.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="updateType">Type of update event</param>
        public PlayerUpdateEventArgs(int playerId, PlayerUpdate updateType)
            : base(playerId) => this.UpdateType = updateType;

        /// <summary>
        /// Gets the type of update
        /// </summary>
        public PlayerUpdate UpdateType { get; private set; }
    }
}
