﻿// <copyright file="PlayerDeathEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using Vcmp.Cshim.Enumerations;

    /// <summary>
    /// Event arguments for OnPlayerDeath
    /// </summary>
    public class PlayerDeathEventArgs : PlayerEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerDeathEventArgs"/> class.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="killerId">The killer's ID</param>
        /// <param name="reason">The reason for death</param>
        /// <param name="bodyPart">The body part shot</param>
        public PlayerDeathEventArgs(int playerId, int killerId, int reason, BodyPart bodyPart)
            : base(playerId)
        {
            this.KillerId = killerId;
            this.Reason = reason;
            this.BodyPart = bodyPart;
        }

        /// <summary>
        /// Gets the killer's ID
        /// </summary>
        public int KillerId { get; private set; }

        /// <summary>
        /// Gets the reason for death
        /// </summary>
        public int Reason { get; private set; }

        /// <summary>
        /// Gets the body part shot
        /// </summary>
        public BodyPart BodyPart { get; private set; }
    }
}
