﻿// <copyright file="PlayerEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    using System;

    /// <summary>
    /// Event arguments for a player event
    /// </summary>
    public class PlayerEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerEventArgs"/> class.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        public PlayerEventArgs(int playerId) => this.PlayerId = playerId;

        /// <summary>
        /// Gets the player's ID
        /// </summary>
        public int PlayerId { get; private set; }
    }
}
