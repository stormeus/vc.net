﻿// <copyright file="PlayerRequestClassEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    /// <summary>
    /// Event arguments for OnPlayerRequestClass
    /// </summary>
    public class PlayerRequestClassEventArgs : CancellablePlayerEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerRequestClassEventArgs"/> class.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        /// <param name="offset">The offset of the class</param>
        public PlayerRequestClassEventArgs(int playerId, int offset)
            : base(playerId) => this.Offset = offset;

        /// <summary>
        /// Gets the offset of the class index
        /// </summary>
        public int Offset { get; private set; }
    }
}
