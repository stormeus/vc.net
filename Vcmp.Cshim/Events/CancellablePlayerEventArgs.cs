﻿// <copyright file="CancellablePlayerEventArgs.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Events
{
    /// <summary>
    /// Event arguments for cancellable player events
    /// </summary>
    public class CancellablePlayerEventArgs : CancellableEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CancellablePlayerEventArgs"/> class.
        /// </summary>
        /// <param name="playerId">The player's ID</param>
        public CancellablePlayerEventArgs(int playerId)
            : base()
        {
            this.PlayerId = playerId;
        }

        /// <summary>
        /// Gets the player's ID
        /// </summary>
        public int PlayerId { get; private set; }
    }
}
