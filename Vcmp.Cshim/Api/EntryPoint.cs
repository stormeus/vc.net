﻿// <copyright file="EntryPoint.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Api
{
    using System;

    /// <summary>
    /// An attribute for other assemblies to specify the entry point for the
    /// C# server director to load it.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class EntryPoint : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntryPoint"/> class.
        /// </summary>
        public EntryPoint()
        {
        }
    }
}
