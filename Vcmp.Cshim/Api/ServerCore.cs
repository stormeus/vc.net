﻿// <copyright file="ServerCore.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Api
{
    using System;
    using Vcmp.Cshim.Enumerations;
    using Vcmp.Cshim.Exceptions;
    using Vcmp.Cshim.Internal;
    using Vcmp.Cshim.Models;

    public class ServerCore
    {
        /// <summary>
        /// Gets the netgame version.
        /// </summary>
        /// <remarks>
        /// This does not correspond to the server versioning scheme (e.g.
        /// 0.4.x) but monotonically increases on updates to the networking
        /// code. Increments of 1000 indicate incompatibility with other
        /// netgame versions and will cause a client error (e.g. clients with
        /// netgame version 65000 cannot connect to servers with netgame
        /// version 64000 or 66000).
        /// </remarks>
        public uint NetgameVersion
        {
            get => VcmpServer.Functions.GetServerVersion();
        }

        /// <summary>
        /// Gets the values of some server settings and stores them in the
        /// <see cref="ServerSettings"/> instance referenced.
        /// </summary>
        public ServerSettings Settings
        {
            get
            {
                var s = default(ServerSettings);
                VcmpServer.Functions.GetServerSettings(ref s);
                return s;
            }
        }

        /// <summary>
        /// Gets the number of server plugins loaded.
        /// </summary>
        /// <remarks>
        /// This does not include .NET assemblies for VC.NET and only refers to
        /// core plugins built against the C SDK header.
        /// </remarks>
        public uint PluginCount
        {
            get => VcmpServer.Functions.GetNumberOfPlugins();
        }

        /// <summary>
        /// Gets the current tick count used by the networking code.
        /// </summary>
        /// <remarks>
        /// This corresponds to QueryPerformanceCounter in the Win32 API.
        /// This does not correspond to clock time.
        /// </remarks>
        public ulong TickCount
        {
            get => VcmpServer.Functions.GetTime();
        }

        /// <summary>
        /// Gets the last <see cref="ErrorCode"/> logged by a plugin call, or
        /// <see cref="ErrorCode.None"/> if no errors have been recorded.
        /// </summary>
        public ErrorCode LastError
        {
            get => VcmpServer.Functions.GetLastError();
        }

        /// <summary>
        /// Gets detailed information about a plugin.
        /// </summary>
        /// <param name="pluginId">The ID for the plugin to get info of</param>
        /// <returns>
        /// A <see cref="PluginInfo"/> struct containing plugin details.
        /// </returns>
        /// <exception cref="UnknownEntityException">
        /// Thrown when no plugin with the given ID can be found
        /// </exception>
        public PluginInfo GetPluginInfo(int pluginId)
        {
            var info = default(PluginInfo);
            var res = VcmpServer.Functions.GetPluginInfo(pluginId, ref info);

            if (res == ErrorCode.NoSuchEntity)
            {
                var message = string.Format(
                    "Could not find a plugin with ID {0}",
                    pluginId);

                throw new UnknownEntityException(message);
            }

            return info;
        }
    }
}
