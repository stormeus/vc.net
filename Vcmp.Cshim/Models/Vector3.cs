﻿// <copyright file="Vector.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public struct Vector3 : IEquatable<Vector3>
    {
        /// <summary>
        /// Gets or sets the X-axis value of the vector
        /// </summary>
        public float X;

        /// <summary>
        /// Gets or sets the Y-axis value of the vector
        /// </summary>
        public float Y;

        /// <summary>
        /// Gets or sets the Z-axis value of the vector
        /// </summary>
        public float Z;

        static Vector3()
        {
            Zero = new Vector3(0.0f, 0.0f, 0.0f);
        }

        public Vector3(float x, float y, float z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public Vector3(Vector3 other)
        {
            this.X = other.X;
            this.Y = other.Y;
            this.Z = other.Z;
        }

        public Vector3(float f)
        {
            this.X = f;
            this.Y = f;
            this.Z = f;
        }

        /// <summary>
        /// Gets a zeroed out vector
        /// </summary>
        public static Vector3 Zero
        {
            get;
            private set;
        }

        public float Length
        {
            get
            {
                float x2 = MathF.Pow(this.X, 2);
                float y2 = MathF.Pow(this.Y, 2);
                float z2 = MathF.Pow(this.Z, 2);
                return MathF.Sqrt(x2 + y2 + z2);
            }
        }

        public Vector3 Normalized
        {
            get
            {
                Vector3 copy = new Vector3(this);
                float invLen = 1.0f / this.Length;

                copy.X *= invLen;
                copy.Y *= invLen;
                copy.Z *= invLen;

                return copy;
            }
        }

        public static Vector3 operator +(Vector3 l, Vector3 r)
        {
            float x = l.X + r.X;
            float y = l.Y + r.Y;
            float z = l.Z + r.Z;
            return new Vector3(x, y, z);
        }

        public static Vector3 operator +(Vector3 v)
        {
            return new Vector3(v);
        }

        public static Vector3 operator -(Vector3 l, Vector3 r)
        {
            float x = l.X - r.X;
            float y = l.Y - r.Y;
            float z = l.Z - r.Z;
            return new Vector3(x, y, z);
        }

        public static Vector3 operator -(Vector3 v)
        {
            float x = -v.X;
            float y = -v.Y;
            float z = -v.Z;
            return new Vector3(x, y, z);
        }

        public static Vector3 operator *(Vector3 l, Vector3 r)
        {
            float x = (l.Y * r.Z) - (l.Z * r.Y);
            float y = (l.Z * r.X) - (l.X * r.Z);
            float z = (l.X * r.Y) - (l.Y * r.X);
            return new Vector3(x, y, z);
        }

        public static Vector3 operator *(Vector3 l, float f)
        {
            float x = l.X * f;
            float y = l.Y * f;
            float z = l.Z * f;
            return new Vector3(x, y, z);
        }

        public static Vector3 operator /(Vector3 l, float f)
        {
            float x = l.X / f;
            float y = l.Y / f;
            float z = l.Z / f;
            return new Vector3(x, y, z);
        }

        public float DistanceTo(Vector3 v)
        {
            float x2 = MathF.Pow(this.X - v.X, 2);
            float y2 = MathF.Pow(this.Y - v.Y, 2);
            float z2 = MathF.Pow(this.Z - v.Z, 2);
            return MathF.Sqrt(x2 + y2 + z2);
        }

        public float Dot(Vector3 v)
        {
            float x = this.X * v.X;
            float y = this.Y * v.Y;
            float z = this.Z * v.Z;
            return x + y + z;
        }

        public bool Equals(Vector3 other)
        {
            return other.X == this.X &&
                   other.Y == this.Y &&
                   other.Z == this.Z;
        }

        public override bool Equals(object obj)
        {
            return obj is Vector3 && this.Equals((Vector3)obj);
        }

        public override int GetHashCode()
        {
            int x = BitConverter.SingleToInt32Bits(this.X);
            int y = BitConverter.SingleToInt32Bits(this.Y);
            int z = BitConverter.SingleToInt32Bits(this.Z);
            return x ^ y ^ z;
        }

        public override string ToString()
        {
            return string.Format("({0}, {1}, {2})", this.X, this.Y, this.Z);
        }
    }
}
