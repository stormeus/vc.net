﻿// <copyright file="Player.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Vcmp.Cshim.Api;
    using Vcmp.Cshim.Enumerations;
    using Vcmp.Cshim.Exceptions;
    using Vcmp.Cshim.Models;

    public class Player
    {
        private static PluginFuncs Functions = VcmpServer.Functions;
        private bool _disposed;
        private int _id;

        public Player(int id)
        {
            if (Functions.IsPlayerConnected(id) == 0)
            {
                throw new UnknownEntityException(string.Format(
                    "Player #{0} is not connected but tried to construct player entity.",
                    id));
            }

            this._id = id;
            this._disposed = false;
        }

        public int Action
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerAction(this._id);
            }
        }

        public Vector3 AimDirection
        {
            get
            {
                this.ValidateDisposalStatus();

                float x = 0.0f, y = 0.0f, z = 0.0f;
                Functions.GetPlayerAimDirection(this._id, ref x, ref y, ref z);

                return new Vector3(x, y, z);
            }
        }

        public Vector3 AimPosition
        {
            get
            {
                this.ValidateDisposalStatus();

                float x = 0.0f, y = 0.0f, z = 0.0f;
                Functions.GetPlayerAimPosition(this._id, ref x, ref y, ref z);

                return new Vector3(x, y, z);
            }
        }

        public int Alpha
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerAlpha(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerAlpha(this._id, value, 0);
            }
        }

        public float Armour
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerArmour(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerArmour(this._id, value);
            }
        }

        public int Class
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerClass(this._id);
            }
        }

        public Colour Colour
        {
            get
            {
                this.ValidateDisposalStatus();

                var colour = new Colour(0);
                colour.RgbaPackedValue = Functions.GetPlayerColour(this._id);
                return colour;
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerColour(this._id, value.RgbaPackedValue);
            }
        }

        public double Fps
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerFPS(this._id);
            }
        }

        public uint GameKeys
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerGameKeys(this._id);
            }
        }

        public float Heading
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerHeading(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerHeading(this._id, value);
            }
        }

        public float Health
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerHealth(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerHealth(this._id, value);
            }
        }

        public int Id
        {
            get
            {
                this.ValidateDisposalStatus();
                return this._id;
            }
        }

        public uint ImmunityFlags
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerImmunityFlags(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerImmunityFlags(this._id, value);
            }
        }
        
        public string IpAddress
        {
            get
            {
                this.ValidateDisposalStatus();

                StringBuilder buffer = new StringBuilder(128);
                Functions.GetPlayerIP(this._id, buffer, 128);
                return buffer.ToString();
            }
        }

        public bool IsAdmin
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.IsPlayerAdmin(this._id) == 1;
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerAdmin(this._id, Convert.ToByte(value));
            }
        }

        public bool IsCrouching
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.IsPlayerCrouching(this._id) == 1;
            }
        }

        public bool IsOnFire
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.IsPlayerOnFire(this._id) == 1;
            }
        }

        public bool IsSpawned
        {
            get
            {
                this.ValidateDisposalStatus();
                return Convert.ToBoolean(Functions.IsPlayerSpawned(this._id));
            }

            set
            {
                this.ValidateDisposalStatus();

                if (value)
                {
                    Functions.ForcePlayerSpawn(this._id);
                }
                else
                {
                    Functions.ForcePlayerSelect(this._id);
                }
            }
        }

        public bool IsTyping
        {
            get
            {
                this.ValidateDisposalStatus();
                return Convert.ToBoolean(Functions.IsPlayerTyping(this._id));
            }
        }

        public uint Key
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerKey(this._id);
            }
        }

        public string MachineId
        {
            get
            {
                this.ValidateDisposalStatus();

                StringBuilder buffer = new StringBuilder(128);
                Functions.GetPlayerUID(this._id, buffer, 128);
                return buffer.ToString();
            }
        }

        public string MachineIdV2
        {
            get
            {
                this.ValidateDisposalStatus();

                StringBuilder buffer = new StringBuilder(128);
                Functions.GetPlayerUID2(this._id, buffer, 128);
                return buffer.ToString();
            }
        }

        public int Money
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerMoney(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerMoney(this._id, value);
            }
        }

        public string Name
        {
            get
            {
                this.ValidateDisposalStatus();

                StringBuilder buffer = new StringBuilder(128);
                Functions.GetPlayerName(this._id, buffer, 128);
                return buffer.ToString();
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerName(this._id, value);
            }
        }

        public int Ping
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerPing(this._id);
            }
        }

        public Vector3 Position
        {
            get
            {
                this.ValidateDisposalStatus();

                float x = 0.0f, y = 0.0f, z = 0.0f;
                Functions.GetPlayerPosition(this._id, ref x, ref y, ref z);

                return new Vector3(x, y, z);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerPosition(this._id, value.X, value.Y, value.Z);
            }
        }

        public int Score
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerScore(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerScore(this._id, value);
            }
        }

        public int SecondaryWorld
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerSecondaryWorld(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerSecondaryWorld(this._id, value);
            }
        }

        public int Skin
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerSkin(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerSkin(this._id, value);
            }
        }

        public Vector3 Speed
        {
            get
            {
                this.ValidateDisposalStatus();

                float x = 0.0f, y = 0.0f, z = 0.0f;
                Functions.GetPlayerSpeed(this._id, ref x, ref y, ref z);

                return new Vector3(x, y, z);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerSpeed(this._id, value.X, value.Y, value.Z);
            }
        }

        public PlayerState State
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerState(this._id);
            }
        }

        public int Team
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerTeam(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerTeam(this._id, value);
            }
        }
        
        public int UniqueWorld
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerUniqueWorld(this._id);
            }
        }

        public int WantedLevel
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerWantedLevel(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerWantedLevel(this._id, value);
            }
        }

        public int World
        {
            get
            {
                this.ValidateDisposalStatus();
                return Functions.GetPlayerWorld(this._id);
            }

            set
            {
                this.ValidateDisposalStatus();
                Functions.SetPlayerWorld(this._id, value);
            }
        }

        internal void Shutdown()
        {
            _disposed = true;
        }

        public void Ban()
        {
            this.ValidateDisposalStatus();
            Functions.BanPlayer(this._id);
        }

        public void ForceSelect()
        {
            this.ValidateDisposalStatus();
            Functions.ForcePlayerSelect(this._id);
        }

        public void ForceSpawn()
        {
            this.ValidateDisposalStatus();
            Functions.ForcePlayerSpawn(this._id);
        }

        public bool GetOption(PlayerOption option)
        {
            this.ValidateDisposalStatus();
            return Convert.ToBoolean(Functions.GetPlayerOption(this._id, option));
        }

        public bool IsStreamedForPlayer(Player other)
        {
            this.ValidateDisposalStatus();
            return Convert.ToBoolean(Functions.IsPlayerStreamedForPlayer(other.Id, this._id));
        }

        public bool IsCompatibleWithWorld(int world)
        {
            this.ValidateDisposalStatus();
            return Convert.ToBoolean(Functions.IsPlayerWorldCompatible(this._id, world));
        }

        public void Kick()
        {
            this.ValidateDisposalStatus();
            Functions.KickPlayer(this._id);
        }

        public void SetAlpha(int alpha, uint fadeTime)
        {
            this.ValidateDisposalStatus();
            Functions.SetPlayerAlpha(this._id, alpha, fadeTime);
        }

        public void SetOption(PlayerOption option, bool toggle)
        {
            this.ValidateDisposalStatus();
            Functions.SetPlayerOption(this._id, option, Convert.ToByte(toggle));
        }

        private void ValidateDisposalStatus()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(string.Format(
                    "Player[{0}]",
                    this._id));
            }
        }
    }
}
