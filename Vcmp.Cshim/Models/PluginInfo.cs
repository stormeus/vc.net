﻿// <copyright file="PluginInfo.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Models
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// Information about this plugin provided to the server
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct PluginInfo
    {
        /// <summary>
        /// The size of this structure in bytes as provided by the server
        /// </summary>
        public uint StructSize;

        /// <summary>
        /// A unique identifier for this plugin
        /// </summary>
        public int PluginId;

        /// <summary>
        /// A name for this plugin
        /// </summary>
        public fixed byte Name[32];

        /// <summary>
        /// The version of this plugin
        /// </summary>
        public uint PluginVersion;

        /// <summary>
        /// The API major version this plugin is compatible with
        /// </summary>
        public short ApiMajorVersion;

        /// <summary>
        /// The API minor version this plugin is compatible with
        /// </summary>
        public short ApiMinorVersion;
    }
}
