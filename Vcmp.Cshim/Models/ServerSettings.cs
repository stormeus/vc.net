﻿// <copyright file="ServerSettings.cs" company="Vice City: Multiplayer Community">
// Copyright (c) Vice City: Multiplayer Community. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.
// </copyright>

namespace Vcmp.Cshim.Models
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// A structure containing the server's active settings
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct ServerSettings
    {
        /// <summary>
        /// The size of the structure as provided by the server
        /// </summary>
        public uint StructSize;

        /// <summary>
        /// The name of the server
        /// </summary>
        public fixed byte ServerName[128];

        /// <summary>
        /// The maximum number of players that can join
        /// </summary>
        public uint MaxPlayers;

        /// <summary>
        /// The server's port
        /// </summary>
        public uint Port;

        /// <summary>
        /// A field of binary flags
        /// </summary>
        public uint Flags;
    }
}
